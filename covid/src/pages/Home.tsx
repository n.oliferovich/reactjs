import { useEffect, useMemo, useState } from "react";
import { GlobalInfoItem } from "../components/GlobalInfoItem";
import { InfoItem } from "../components/InfoItem";
import { SummaryResponse } from "../http/responses/CovidResponse";
import CovidService from "../http/services/CovidService";

export const Home = () => {
  const [summary, setSummary] = useState<SummaryResponse>();

  const countriesInfo = useMemo(() => {
    return summary?.Countries.map((country) => {
      return <InfoItem key={country.ID} country={country} />;
    });
  }, [summary]);

  async function getSummaryInfo() {
    try {
      const res = await CovidService.getSummary();
      setSummary(res.data);
    } catch (e: any) {
      console.error(e);
    }
  }

  useEffect(() => {
    getSummaryInfo();
  }, []);

  return (
    <div className="home">
      <div className="countries-info">
        {summary?.Global && <GlobalInfoItem global={summary.Global} />}
        {countriesInfo}
      </div>
    </div>
  );
};
