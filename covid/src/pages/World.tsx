import { useEffect, useState } from "react";
import dateFormat from "dateformat";
import { GlobalChart } from "../components/GlobalChart";
import {
  convertedGlobalResponse,
  GlobalSummaryResponse,
} from "../http/responses/CovidResponse";
import CovidService from "../http/services/CovidService";
import "./Country.css";

export const World = () => {
  const [globalInfo, setGlobalInfo] = useState<Array<convertedGlobalResponse>>(
    []
  );
  let key = 0;

  async function getTotal() {
    try {
      const res = await CovidService.getTotalWorld();
      const formatedData: convertedGlobalResponse[] = res.data.map(
        (day: GlobalSummaryResponse) => {
          return {
            ...day,
            Date: dateFormat(day.Date, "mmmm dS, yyyy"),
          };
        }
      );
      setGlobalInfo(formatedData);
      key = res.data.length - 1;
    } catch (e: any) {
      console.error(e);
    }
  }

  useEffect(() => {
    getTotal();
  }, []);

  return (
    <div className="country">
      <h2>World</h2>
      {globalInfo.length > 0 ? (
        <div className="info-item">
          <div className="info-details row">
            <div className="detail col s12 m4">
              <div className="detail-item">
                <p>
                  <strong>New Confirmed</strong>
                </p>
                <p>{globalInfo[key].NewConfirmed}</p>
              </div>
              <div className="detail-item">
                <p>
                  <strong>Total Confirmed</strong>
                </p>
                <p>{globalInfo[key].TotalConfirmed}</p>
              </div>
            </div>
            <div className="detail col s12 m4">
              <div className="detail-item">
                <p>
                  <strong>New Deaths</strong>
                </p>
                <p>{globalInfo[key].NewDeaths}</p>
              </div>
              <div className="detail-item">
                <p>
                  <strong>Total Deaths</strong>
                </p>
                <p>{globalInfo[key].TotalDeaths}</p>
              </div>
            </div>
            <div className="detail col s12 m4">
              <div className="detail-item">
                <p>
                  <strong>New Recovered</strong>
                </p>
                <p>{globalInfo[key].NewRecovered}</p>
              </div>
              <div className="detail-item">
                <p>
                  <strong>Total Recovered</strong>
                </p>
                <p>{globalInfo[key].TotalRecovered}</p>
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}

      <h4>New Confirmed</h4>
      {globalInfo.length > 0 ? (
        <GlobalChart dataKey="NewConfirmed" data={globalInfo} />
      ) : (
        <p>No results</p>
      )}
      <h4>Total Confirmed</h4>
      {globalInfo.length > 0 ? (
        <GlobalChart dataKey="TotalConfirmed" data={globalInfo} />
      ) : (
        <p>No results</p>
      )}
      <h4>New Deaths</h4>
      {globalInfo.length > 0 ? (
        <GlobalChart dataKey="NewDeaths" data={globalInfo} />
      ) : (
        <p>No results</p>
      )}
      <h4>Total Deaths</h4>
      {globalInfo.length > 0 ? (
        <GlobalChart dataKey="TotalDeaths" data={globalInfo} />
      ) : (
        <p>No results</p>
      )}
      <h4>New Recovered</h4>
      {globalInfo.length > 0 ? (
        <GlobalChart dataKey="NewRecovered" data={globalInfo} />
      ) : (
        <p>No results</p>
      )}
      <h4>Total Recovered</h4>
      {globalInfo.length > 0 ? (
        <GlobalChart dataKey="TotalRecovered" data={globalInfo} />
      ) : (
        <p>No results</p>
      )}
    </div>
  );
};
