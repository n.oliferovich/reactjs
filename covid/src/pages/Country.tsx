import { AxiosResponse } from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import dateFormat from "dateformat";
import Chart from "../components/Chart";
import { CountryResponse } from "../http/responses/CountryResponse";
import {
  convertedCountryTotalResponse,
  CountryTotalResponse,
} from "../http/responses/CovidResponse";
import CountriesService from "../http/services/CountriesService";
import CovidService from "../http/services/CovidService";
import "./Country.css";

export const Country = () => {
  const { id } = useParams();
  const [name, setName] = useState("");
  const [countryInfo, setCountryInfo] = useState<
    Array<convertedCountryTotalResponse>
  >([]);
  let key = 0;

  async function getTotal() {
    try {
      if (id) {
        const res = await CovidService.getTotalByCountry(id);
        const formatedData: convertedCountryTotalResponse[] = res.data.map(
          (day: CountryTotalResponse) => {
            return {
              ...day,
              Date: dateFormat(day.Date, "mmmm dS, yyyy"),
            };
          }
        );
        setCountryInfo(formatedData);
        key = res.data.length - 1;
      }
    } catch (e: any) {
      console.error(e);
    }
  }

  useEffect(() => {
    getTotal();
  }, [id]);

  useEffect(() => {
    async function setCountries() {
      try {
        await CountriesService.getCountries().then(
          (res: AxiosResponse<CountryResponse[]>) => {
            const country = res.data.find((country) => country.Slug === id);
            if (country) setName(country.Country);
          }
        );
      } catch (e: any) {
        console.error(e);
      }
    }

    setCountries();
  }, [id]);

  return (
    <div className="country">
      <h2>{name}</h2>
      {countryInfo.length > 0 ? (
        <div className="info-item">
          <div className="info-details row">
            <div className="detail col s12 m3">
              <div className="detail-item">
                <p>
                  <strong>Confirmed</strong>
                </p>
                <p>{countryInfo[key].Confirmed}</p>
              </div>
            </div>
            <div className="detail col s12 m3">
              <div className="detail-item">
                <p>
                  <strong>Deaths</strong>
                </p>
                <p>{countryInfo[key].Deaths}</p>
              </div>
            </div>
            <div className="detail col s12 m3">
              <div className="detail-item">
                <p>
                  <strong>Recovered</strong>
                </p>
                <p>{countryInfo[key].Recovered}</p>
              </div>
            </div>
            <div className="detail col s12 m3">
              <div className="detail-item">
                <p>
                  <strong>Active</strong>
                </p>
                <p>{countryInfo[key].Active}</p>
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}

      <h4>Confirmed</h4>
      {countryInfo.length > 0 ? (
        <Chart dataKey="Confirmed" data={countryInfo} />
      ) : (
        <p>No results</p>
      )}
      <h4>Deaths</h4>
      {countryInfo.length > 0 ? (
        <Chart dataKey="Deaths" data={countryInfo} />
      ) : (
        <p>No results</p>
      )}
      <h4>Recovered</h4>
      {countryInfo.length > 0 ? (
        <Chart dataKey="Recovered" data={countryInfo} />
      ) : (
        <p>No results</p>
      )}
      <h4>Active</h4>
      {countryInfo.length > 0 ? (
        <Chart dataKey="Active" data={countryInfo} />
      ) : (
        <p>No results</p>
      )}
    </div>
  );
};
