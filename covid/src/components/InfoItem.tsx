import { NavLink } from "react-router-dom";
import dateFormat from "dateformat";
import { SummaryCountryResponse } from "../http/responses/CovidResponse";
import "./InfoItem.css";

export const InfoItem = ({ country }: { country: SummaryCountryResponse }) => {
  return (
    <NavLink to={`/${country.Slug}`} className="info-item">
      <h4 className="info-title">{country.Country}</h4>
      <p>{dateFormat(country.Date, "mmmm dS, yyyy")}</p>
      <div className="info-details row">
        <div className="detail col s12 m3"></div>
        <div className="detail col s12 m3">
          <div className="detail-item">
            <p>
              <strong>New Confirmed</strong>
            </p>
            <p>{country.NewConfirmed}</p>
          </div>
          <div className="detail-item">
            <p>
              <strong>Total Confirmed</strong>
            </p>
            <p>{country.TotalConfirmed}</p>
          </div>
        </div>
        <div className="detail col s12 m3">
          <div className="detail-item">
            <p>
              <strong>New Deaths</strong>
            </p>
            <p>{country.NewDeaths}</p>
          </div>
          <div className="detail-item">
            <p>
              <strong>Total Deaths</strong>
            </p>
            <p>{country.TotalDeaths}</p>
          </div>
        </div>
        <div className="detail col s12 m3">
          <div className="detail-item">
            <p>
              <strong>New Recovered</strong>
            </p>
            <p>{country.NewRecovered}</p>
          </div>
          <div className="detail-item">
            <p>
              <strong>Total Recovered</strong>
            </p>
            <p>{country.TotalRecovered}</p>
          </div>
        </div>
      </div>
    </NavLink>
  );
};
