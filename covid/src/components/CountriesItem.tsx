import { NavLink, useParams } from "react-router-dom";
import { CountryResponse } from "../http/responses/CountryResponse";
import "./CountriesItem.css";

export const CountriesItem = ({ country }: { country: CountryResponse }) => {
  const { id } = useParams();

  return (
    <div className="country-item">
      <NavLink
        to={`/${country.Slug}`}
        className={`${id === country.Slug ? "active" : ""}`}
      >
        {country.Country}
      </NavLink>
    </div>
  );
};
