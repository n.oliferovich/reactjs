import { NavLink } from "react-router-dom";
import dateFormat from "dateformat";
import { GlobalSummaryResponse } from "../http/responses/CovidResponse";
import "./InfoItem.css";

export const GlobalInfoItem = ({
  global,
}: {
  global: GlobalSummaryResponse;
}) => {
  return (
    <NavLink to={`/world`} className="info-item">
      <h4 className="info-title">World</h4>
      <p>{dateFormat(global.Date, "mmmm dS, yyyy")}</p>
      <div className="info-details row">
        <div className="detail col s12 m3"></div>
        <div className="detail col s12 m3">
          <div className="detail-item">
            <p>
              <strong>New Confirmed</strong>
            </p>
            <p>{global.NewConfirmed}</p>
          </div>
          <div className="detail-item">
            <p>
              <strong>Total Confirmed</strong>
            </p>
            <p>{global.TotalConfirmed}</p>
          </div>
        </div>
        <div className="detail col s12 m3">
          <div className="detail-item">
            <p>
              <strong>New Deaths</strong>
            </p>
            <p>{global.NewDeaths}</p>
          </div>
          <div className="detail-item">
            <p>
              <strong>Total Deaths</strong>
            </p>
            <p>{global.TotalDeaths}</p>
          </div>
        </div>
        <div className="detail col s12 m3">
          <div className="detail-item">
            <p>
              <strong>New Recovered</strong>
            </p>
            <p>{global.NewRecovered}</p>
          </div>
          <div className="detail-item">
            <p>
              <strong>Total Recovered</strong>
            </p>
            <p>{global.TotalRecovered}</p>
          </div>
        </div>
      </div>
    </NavLink>
  );
};
