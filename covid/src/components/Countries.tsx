import { AxiosResponse } from "axios";
import { FC, useEffect, useMemo, useState } from "react";
import { CountryResponse } from "../http/responses/CountryResponse";
import CountriesService from "../http/services/CountriesService";
import { CountriesItem } from "./CountriesItem";

export const Countries: FC = () => {
  const [countrylist, setCountryList] = useState<Array<CountryResponse>>([]);
  const [list, setList] = useState<Array<CountryResponse>>([]);
  const [name, setName] = useState("");

  const countries = useMemo(() => {
    return countrylist.map((country: CountryResponse) => {
      return <CountriesItem key={country.ISO2} country={country} />;
    });
  }, [countrylist]);

  const search = (value: string) => {
    const searchCountries = list.filter((country) =>
      country.Country.toLowerCase().includes(value.toLowerCase())
    );
    setCountryList(searchCountries);
  };

  const searchHandler = (e: React.FormEvent<HTMLInputElement>) => {
    const value = e.currentTarget.value;
    if (!value) {
      setName("");
      setCountryList(list);
      return;
    }
    setName(value);
    search(value);
  };

  async function setCountries() {
    try {
      await CountriesService.getCountries().then(
        (res: AxiosResponse<CountryResponse[]>) => {
          setList(res.data);
          let dataSort = res.data.sort(function (a, b) {
            return a.Country.localeCompare(b.Country);
          });
          dataSort.unshift({
            Country: "World",
            Slug: "world",
            ISO2: "world",
          });
          setCountryList(dataSort);
        }
      );
    } catch (e: any) {
      console.error(e);
    }
  }

  useEffect(() => {
    setCountries();
  }, []);

  return (
    <div className="country-display">
      <div className="search-form">
        <div className="input-field">
          <input
            placeholder="Country"
            id="name"
            type="text"
            value={name}
            onChange={searchHandler}
          />
          <label htmlFor="name" className="active">
            Search
          </label>
        </div>
      </div>
      <div className="countries">{countries}</div>
    </div>
  );
};
