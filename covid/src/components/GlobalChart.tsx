import {
  Area,
  AreaChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { convertedGlobalResponse } from "../http/responses/CovidResponse";
import "./Chart.css";

export const GlobalChart = ({
  dataKey,
  data,
}: {
  dataKey: string;
  data: Array<convertedGlobalResponse>;
}) => {
  return (
    <ResponsiveContainer width="100%" height={300}>
      <AreaChart data={data}>
        <Area dataKey={dataKey} />
        <XAxis dataKey="Date" />
        <YAxis dataKey={dataKey} />
        <Tooltip />
      </AreaChart>
    </ResponsiveContainer>
  );
};
