import { NavLink } from "react-router-dom";

export const Header = () => {
  return (
    <nav>
      <div className="nav-wrapper blue darken-1">
        <NavLink to="/" className="brand-logo">
          COVID19
        </NavLink>
        <ul id="nav-mobile" className="right hide-on-med-and-down">
          <li>
            <NavLink to="/">Home</NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};
