import {
  Area,
  AreaChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { convertedCountryTotalResponse } from "../http/responses/CovidResponse";
import "./Chart.css";

const Chart = ({
  dataKey,
  data,
}: {
  dataKey: string;
  data: Array<convertedCountryTotalResponse>;
}) => {
  return (
    <ResponsiveContainer width="100%" height={300}>
      <AreaChart data={data}>
        <Area dataKey={dataKey} />
        <XAxis dataKey="Date" />
        <YAxis dataKey={dataKey} />
        <Tooltip />
      </AreaChart>
    </ResponsiveContainer>
  );
};

export default Chart;
