export interface CountryResponse {
  Country: string;
  Slug: string;
  ISO2: string;
}
