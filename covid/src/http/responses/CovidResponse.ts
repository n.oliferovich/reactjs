export interface GlobalResponse {
  NewConfirmed: number;
  TotalConfirmed: number;
  NewDeaths: number;
  TotalDeaths: number;
  NewRecovered: number;
  TotalRecovered: number;
}

export interface GlobalSummaryResponse extends GlobalResponse {
  Date: Date;
}

export interface convertedGlobalResponse extends GlobalResponse {
  Date: string;
}

export interface SummaryResponse {
  ID: string;
  Message: string;
  Global: GlobalSummaryResponse;
  Countries: SummaryCountryResponse[];
}

export interface SummaryCountryResponse {
  ID: string;
  Country: string;
  CountryCode: string;
  Slug: string;
  NewConfirmed: number;
  TotalConfirmed: number;
  NewDeaths: number;
  TotalDeaths: number;
  NewRecovered: number;
  TotalRecovered: 0;
  Date: Date;
  Premium: {};
}

export interface CovidResponse {
  Country: string;
  CountryCode: string;
  Province: string;
  City: string;
  CityCode: string;
  Lat: string;
  Lon: string;
}

export interface CovidTotalResponse extends CovidResponse {
  Cases: number;
  Status: string;
  Date: Date;
}

export interface CountryTotalResponse extends CovidResponse {
  ID: string;
  Confirmed: number;
  Deaths: number;
  Recovered: number;
  Active: number;
  Date: Date;
}

export interface convertedCountryTotalResponse extends CovidResponse {
  ID: string;
  Confirmed: number;
  Deaths: number;
  Recovered: number;
  Active: number;
  Date: string;
}
