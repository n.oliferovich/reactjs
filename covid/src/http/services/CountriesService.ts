import { AxiosResponse } from "axios";
import api from "..";
import { CountryResponse } from "../responses/CountryResponse";

export default class CountriesService {
  static async getCountries(): Promise<AxiosResponse<Array<CountryResponse>>> {
    return api.get<Array<CountryResponse>>("/countries");
  }
}
