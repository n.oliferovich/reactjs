import { AxiosResponse } from "axios";
import api from "..";
import {
  CountryTotalResponse,
  CovidTotalResponse,
  GlobalSummaryResponse,
  SummaryResponse,
} from "../responses/CovidResponse";

export default class CovidService {
  static async getSummary(): Promise<AxiosResponse<SummaryResponse>> {
    return api.get<SummaryResponse>("/summary");
  }

  static async getTotalConfirmedByCountry(
    country: string
  ): Promise<AxiosResponse<Array<CovidTotalResponse>>> {
    return api.get<Array<CovidTotalResponse>>(
      `/total/country/${country}/status/confirmed?from=2020-03-01T00:00:00Z&to=2022-03-01T00:00:00Z`
    );
  }

  static async getTotalByCountry(
    country: string
  ): Promise<AxiosResponse<Array<CountryTotalResponse>>> {
    return api.get<Array<CountryTotalResponse>>(
      `/live/country/${country}/status/confirmed/date/2021-06-24T00:00:00Z`
    );
  }

  static async getTotalWorld(): Promise<
    AxiosResponse<Array<GlobalSummaryResponse>>
  > {
    return api.get<Array<GlobalSummaryResponse>>(
      `/world?from=2020-03-01T00:00:00Z&to=2022-04-01T00:00:00Z`
    );
  }
}
