import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { Header } from "./components/Header";
import { Footer } from "./components/Footer";
import { Home } from "./pages/Home";
import { Country } from "./pages/Country";
import "material-icons/iconfont/material-icons.css";
import "materialize-css";
import "./App.css";
import { Countries } from "./components/Countries";
import { World } from "./pages/World";

function App() {
  return (
    <div className="App">
      <div className="App__inner">
        <Router>
          <header>
            <Header />
          </header>
          <main>
            <div className="container">
              <div className="row">
                <div className="col s12 m3">
                  <Countries />
                </div>
                <div className="col s12 m9">
                  <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/:id" element={<Country />} />
                    <Route path="/world" element={<World/>} />
                  </Routes>
                </div>
              </div>
            </div>
          </main>
          <footer className="page-footer blue darken-1">
            <Footer />
          </footer>
        </Router>
      </div>
    </div>
  );
}

export default App;
