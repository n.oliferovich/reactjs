public static void findZigZagSequence(int [] a, int n){
        
        Arrays.sort(a);
        int mid = (n - 1)/2; //3
        
        int temp = a[mid]; //4
        a[mid] = a[n - 1];
        a[n - 1] = temp;
        int st = mid + 1; //4
        int ed = n - 1; //6
        while(st < ed) {
            temp = a[st];
            a[st] = a[ed];
            a[ed] = temp;
            st = st + 1;
            ed = ed - 1;
        } 
        for(int i = 0; i < n; i++){            
            System.out.print(a[i]);
            if(i < n) System.out.print(" ");
        }
        System.out.println();
    }