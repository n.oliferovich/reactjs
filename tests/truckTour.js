// The function returns starting point if there is a possible solution,
// otherwise returns -1
function truckTour(arr) {
  let start = 0;
  let end = 1;
  const n = arr.length; //3

  let curr_petrol = arr[0][0] - arr[0][1]; //2

  // If current amount of petrol in truck becomes less than 0, then
  // remove the starting petrol pump from tour
  while (end != start || curr_petrol < 0) {
    // If current amount of petrol in truck becomes less than 0, then
    // remove the starting petrol pump from tour
    while (curr_petrol < 0 && start != end) {
      // Remove starting petrol pump. Change start
      curr_petrol -= arr[start][0] - arr[start][1];
      start = (start + 1) % n;

      // If 0 is being considered as start again, then there is no
      // possible solution
      if (start == 0) return -1;
    }

    // Add a petrol pump to current tour
    curr_petrol += arr[end][0] - arr[end][1];
    end = (end + 1) % n;
  }

  // Return starting point
  return start;
}

console.log(
  truckTour([
    [6, 4],
    [3, 6],
    [7, 3],
  ])
);
