function isBalanced(s) {
  const openers = ["(", "{", "["];
  const closers = {
    ")": "(",
    "}": "{",
    "]": "[",
  };
  const bracket_stack = [];

  for (let i = 0; i < s.length; i++) {
    const c = s.charAt(i);
    if (openers.includes(c)) {
      bracket_stack.push(c);
    } else if (closers[c]) {
      if (bracket_stack.length === 0) {
        return "NO";
      }

      if (bracket_stack.pop() !== closers[c]) {
        return "NO";
      }
    }
  }

  if (bracket_stack.length > 0) {
    return "NO";
  }

  return "YES";
}

console.log(isBalanced('{{[[(())]]}}'))