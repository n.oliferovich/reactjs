// (grid=['eabcd','fghij','olkmn','trpqs','xywuv'])
function gridChallenge(grid = ["mpxz", "abcd", "wlmf"]) {
  let isFalse = false;
  const newGrid = grid.map((str) => {
    const arr = str.split("");
    const sortedArr = arr.sort();

    return sortedArr.join("");
  });
  console.log(newGrid);
  //   newGrid
  //     .join("")
  //     .split("")
  //     .forEach((item, i, arr) => {
  //       if (arr[i] > arr[i + 1]) isFalse = true;
  //     });

  for (let i = 0; i < newGrid.length - 1; i++) {
    for (let j = 0; j < newGrid[i].length; j++) {
    //   console.log(newGrid[i][j] + " vs " + newGrid[i + 1][j]);
      if (newGrid[i][j] > newGrid[i + 1][j]) isFalse = true;
    }
  }

  console.log(isFalse ? "NO" : "YES");

  return newGrid;
}

gridChallenge();
