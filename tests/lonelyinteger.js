function lonelyinteger(a) {
  let uniq;
  let map = new Map();
  a.forEach((item) => {
    let n = map.get(item) || 0;
    map.set(item, ++n);
  });

  for (let [key, value] of map.entries()) {
    if (value === 1) uniq = key;
  }

  return uniq;
}
