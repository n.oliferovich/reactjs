function processData(input) {
  let str = "";
  const arr = input.split("\n");
  const state = [];

  for (let i = 1; i < arr.length; i++) {
    const q = arr[i].split(" ");

    switch (q[0]) {
      case "1":
        str += q[1];
        state.push(str);
        break;
      case "2":
        str = str.slice(0, str.length - q[1]);
        state.push(str);
        break;
      case "3":
        console.log(str[q[1] - 1]);
        break;
      case "4":
        state.pop();
        str = state[state.length - 1];
        break;
    }
  }
}

processData("8\n1 abc\n3 3\n2 3\n1 xy\n3 2\n4\n4\n3 1");
// 8
// 1 abc
// 3 3
// 2 3
// 1 xy
// 3 2
// 4
// 4
// 3 1

// 1. append - Append string  to the end of .
// 2. delete - Delete the last  characters of .
// 3. print - Print the  character of .
// 4. undo
