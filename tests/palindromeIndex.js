function palindromeIndex(s) {
  const arr = s.split("");
  let map = new Map();
  arr.forEach((i) => {
    let count = map.get(i) || 0;
    map.set(i, count + 1);
  });
  let isExist = 0;

  for (let key of map.keys()) {
    console.log(key + "  " + map.get(key));
    if (map.get(key) === 1) isExist = key;
  }

  return isExist ? arr.indexOf(isExist) : -1;
}

console.log(palindromeIndex("babi7loolibab"));

function palindrome(s) {
  const l = s.length;
  let i = 0;
  let j = l - 1;
  while (i < l) {
    if (s[i] != s[j]) break;
    i += 1;
    j -= 1;
  }
  if (i > j) return -1;
  let a = i + 1;
  let b = j;
  while (a < j && b > i + 1) {
    if (s[a] != s[b]) return j;
    a += 1;
    b -= 1;
  }
  return i;
}

console.log(palindrome("babi7loolibab"));
