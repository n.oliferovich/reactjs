function timeConversion(s) {
  const arr = s.split("");
  let hours = s[0] + s[1];
  let newArr = arr.splice(0, arr.length - 2);
  const modifier = arr.splice(-2, 2).join("");
  if (modifier === "PM" && hours !== "12") {
    hours = Number(hours) + 12;
  } else if (modifier === "AM" && hours === "12") {
    hours = "00";
  }
  newArr[1] = hours;
  newArr.shift();
  const result = newArr.join("");

  return result;
}
