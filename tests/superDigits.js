function superDigit2(n, k) {
  const str = n.toString();
  const arr = str.split("");
  let sum = 0;
  arr.forEach((i) => (sum += Number(i)));

  if (k > 0) return superDigit2(sum, sum.toString().length - 1);

  return sum;
}

function superDigit(n, k) {
  const str = n.toString();
  let a = "";

  for (let i = 0; i < k; i++) {
    a += str;
  }

  const sum = superDigit2(a, a.length);

  console.log(sum);

  return sum;
}

superDigit(148, 3);

//const N = require('bignumber.js');

const recursiveFunc = n => n < 10 ? n : superDigit(new N(n).toFixed());

function superDigit(n, k = 1) {
    let i = 0;

    for (const c of n) i += +c;

    return recursiveFunc(i * k);
}

