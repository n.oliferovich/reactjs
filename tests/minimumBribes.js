function minimumBribes(queue) {
  let chaotic = false;
  let bribes = 0;
  for (let i = 0; i < queue.length; i++) {
    if (queue[i] - (i + 1) > 2) {
      chaotic = true;
    }
    for (let j = 0; j < i; j++) {
      if (queue[j] > queue[i]) {
        bribes++;
      }
    }
  }

  if (chaotic) {
    console.log("Too chaotic");
  } else {
    console.log(bribes);
  }
}

minimumBribes([2, 1, 5, 3, 4]);

function minimumBribes2(q) {
    let n = q.length;
    let counter = 0;
    let tracks = Array.from({ length:q.length }, () => 0);
    let swapped;
    do{
        swapped = 0;
        for(let i = 0 ; i < n - 1 ; i++ ){
            let ticketNo = q[i] - 1;
            if(tracks[ticketNo] < 2 && q[i] > q[i+1] ){
                let tempx = q[i+1];
                q[i+1] = q[i];
                q[i] = tempx;

                counter ++;
                tracks[ticketNo] ++;
                swapped++;
            }
        }
    }while( swapped != 0 );
    
    for(let i = 0 ; i < n ; i++){
        if(q[i] != i+1){
            counter = "Too chaotic";
            break;
        }
    }
    console.log(counter);

}

minimumBribes2([2, 1, 5, 3, 4]);