function plusMinus(arr) {
    let pos = 0;
    let neg = 0;
    let zero = 0;
    const l = arr.length;
    arr.forEach(arrTemp => {
        if (arrTemp > 0) {
            pos++;
        } else if (arrTemp < 0) {
            neg++;
        } else {
            zero++;
        }
    })    
    console.log(`${(pos/l).toFixed(6)}
    ${(neg/l).toFixed(6)}
    ${(zero/l).toFixed(6)}`);

}