function miniMaxSum(arr) {
  let max = Math.max(...arr);
  let min = Math.min(...arr);
  const sum = arr.reduce((sum, current) => {
    return sum + current;
  }, 0);
  
  console.log(sum - max + " " + (sum - min));
}
