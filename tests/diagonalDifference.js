function diagonalDifference(arr) {
  const l = arr.length;
  let diag1 = 0;
  let diag2 = 0;

  for (let i = 0; i < l; i++) {
    for (let j = 0; j < l; j++) {
      if (i === j) {
        diag1 += arr[i][j];
      }
      if (i + j === l - 1) {
        diag2 += arr[i][j];
      }
    }
  }

  return Math.abs(diag1 - diag2);
}
