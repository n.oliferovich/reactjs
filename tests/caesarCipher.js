function caesarCipher(s = "www.abc.xy", k = 87) {
  const n = k - 26 * Math.floor(k / 26);
  const alph = "abcdefghijklmnopqrstuvwxyz";
  const alphBig = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  let alphArr = alph.split("");
  let alphArrBig = alphBig.split("");
  const startArr = alphArr.splice(0, n);
  const startArr2 = alphArrBig.splice(0, n);
  const transformedAlph = alphArr.concat(startArr);
  const transformedAlph2 = alphArrBig.concat(startArr2);
  let result = "";

  for (let i = 0; i < s.length; i++) {
    let key = alph.indexOf(s[i]);
    let key2 = alphBig.indexOf(s[i]);
    if (key < 0 && key2 < 0) {
      result += s[i];
    } else if (key2 < 0) {
      result += transformedAlph[key];
    } else {
      result += transformedAlph2[key2];
    }
  }

  return result;
}

console.log(caesarCipher());
