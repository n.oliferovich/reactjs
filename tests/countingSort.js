const s = [1, 2, 3, 2, 1, 1, 4, 1];

function countingSort(arr) {
  const l = 100;
  const sortArr = Array(l)
    .fill()
    .map((v, i) => i);
  let numArr = Array(l)
    .fill()
    .map((v, i) => 0);
  for (let i = 0; i < l; i++) {
    for (let j = 0; j < arr.length; j++) {
      if (sortArr[i] === arr[j]) {
        numArr[i] += 1;
      }
    }
  }

  return numArr;
}

console.log(countingSort(s));
