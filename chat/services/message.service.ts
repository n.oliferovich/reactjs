import sequelize from '../database/db.config';
import { IMessageService } from './interfaces/message.inteface';

class MessageService implements IMessageService {
  getMessages: IMessageService['getMessages'] = async (ChatId) => {
    const messages = await sequelize.model('Message').findAll({
      where: {
        ChatId,
      },
      order: [['updatedAt', 'ASC']],
    });

    if (!messages) {
      return [];
    }

    return messages;
  };

  addMessage: IMessageService['addMessage'] = async (sender, ChatId, text) => {
    const newMessage = sequelize.model('Message').create({
      sender,
      text,
      ChatId,
    });

    return newMessage;
  };
}

export default new MessageService();
