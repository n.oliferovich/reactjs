import jwt from 'jsonwebtoken';
import { Model } from 'sequelize/types';

export interface RefreshType {
  id: string;
  email: string;
  password: string;
  createdAt: string;
  updatedAt: string;
  iat: number;
  exp: number;
}

export interface ITokenService {
  generateTokens: (payload: { email: string; id: string }) => {
    accessToken: string;
    refreshToken: string;
  };
  validateAccessToken: (token: string) => string | jwt.JwtPayload | null;
  validateRefreshToken: (token: string) => RefreshType | null;
  saveToken: (
    userId: string,
    refreshToken: string
  ) => Promise<Model<any, any> | [affectedCount: number]>;
  removeToken: (refreshToken: string) => Promise<number>;
  findToken: (refreshToken: string) => Promise<Model<any, any> | null>;
}
