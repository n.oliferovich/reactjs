import { Model } from 'sequelize/types';

export interface IGroupService {
  getChats: (id: string) => Promise<Model<any, any>[]>;
  addChat: (members: string[], name: string) => Promise<Model<any, any>>;
}
