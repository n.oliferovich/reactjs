interface IUserToken {
  user: {
    id: string;
    email: string;
  };
  accessToken: string;
  refreshToken: string;
}

export interface IAuthService {
  register: (email: string, password: string) => Promise<IUserToken>;
  login: (email: string, password: string) => Promise<IUserToken>;
  logout: (refreshToken: string) => Promise<number>;
  refresh: (refreshToken: string) => Promise<IUserToken>;
}
