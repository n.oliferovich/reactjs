import { Model } from 'sequelize/types';

export interface IMessageService {
  getMessages: (ChatId: string) => Promise<Model<any, any>[]>;
  addMessage: (
    sender: string,
    ChatId: string,
    text: string
  ) => Promise<Model<any, any>>;
}
