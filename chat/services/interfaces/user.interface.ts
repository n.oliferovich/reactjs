import { Model } from 'sequelize/types';

export interface IUserService {
  getUsersExcludeId: (id: string) => Promise<any[] | Model<any, any>>;
  getUsersExcludeIds: (ids: string[]) => Promise<Model<any, any>[]>;
  getUser: (id: string) => Promise<Model<any, any>>;
  getUsersByIds: (ids: Array<string>) => Promise<Model<any, any>[]>;
}
