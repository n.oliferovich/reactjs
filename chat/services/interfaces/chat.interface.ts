import { Model } from 'sequelize/types';

export interface IChatService {
  getChats: (id: string) => Promise<Array<Model<any, any>>>;
  addChat: (members: Array<string>) => Promise<Model<any, any>>;
  getGroupChats: (id: string) => Promise<Array<Model<any, any>>>;
  addGroupChat: (members: string[], name: string) => Promise<Model<any, any>>;
}
