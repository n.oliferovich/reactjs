import Sequelize from 'sequelize';
import sequelize from '../database/db.config';
import { IUserService } from './interfaces/user.interface';

class UserService implements IUserService {
  getUsersExcludeId: IUserService['getUsersExcludeId'] = async (id) => {
    const users = await sequelize.model('User').findOne({
      where: {
        id: !id,
      },
    });

    if (!users) {
      return [];
    }

    return users;
  };

  getUsersExcludeIds: IUserService['getUsersExcludeIds'] = async (ids) => {
    const users = await sequelize.model('User').findAll({
      where: {
        id: {
          [Sequelize.Op.notIn]: ids,
        },
      },
    });

    if (!users) {
      return [];
    }

    return users;
  };

  getUser: IUserService['getUser'] = async (id) => {
    const user = await sequelize.model('User').findOne({
      where: {
        id,
      },
    });

    if (!user) {
      throw { status: 400, message: `The user doesn't exist` };
    }

    return user;
  };

  getUsersByIds: IUserService['getUsersByIds'] = async (ids) => {
    const users = await sequelize.model('User').findAll({
      where: {
        id: ids,
      },
    });

    if (!users) {
      throw { status: 500, message: `Crashed getting users` };
    }

    return users;
  };
}

export default new UserService();
