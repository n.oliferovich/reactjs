import sequelize from '../database/db.config';
import bcrypt from 'bcrypt';
import { default as tokenService } from './token.service';
import { IAuthService } from './interfaces/auth.interface';

class AuthService implements IAuthService {
  register: IAuthService['register'] = async (email, password) => {
    const candidate = await sequelize.model('User').findOne({
      where: {
        email: email,
      },
    });

    if (candidate) {
      throw { status: 400, message: `The user with this email exists` };
    }

    const hashPassword = await bcrypt.hash(password, 3);
    const user = (
      await sequelize.model('User').create({ email, password: hashPassword })
    ).get({ plain: true });

    const tokens = tokenService.generateTokens({ ...user });

    await tokenService.saveToken(user.id, tokens.refreshToken);

    return {
      ...tokens,
      user: { id: user.id, email: user.email },
    };
  };

  login: IAuthService['login'] = async (email, password) => {
    const user = (
      await sequelize.model('User').findOne({
        where: {
          email,
        },
      })
    )?.get({ plain: true });

    if (!user) {
      throw { status: 404, message: 'User with this email doesn`t exist' };
    }

    const isPassEquels = await bcrypt.compare(password, user.password);

    if (!isPassEquels) {
      throw { status: 400, message: 'Invalid password' };
    }

    const tokens = tokenService.generateTokens(user);
    await tokenService.saveToken(user.id, tokens.refreshToken);

    return {
      ...tokens,
      user: { id: user.id, email: user.email },
    };
  };

  logout: IAuthService['logout'] = async (refreshToken) => {
    const token = await tokenService.removeToken(refreshToken);

    return token;
  };

  refresh: IAuthService['refresh'] = async (refreshToken) => {
    if (!refreshToken) {
      throw { status: 401, message: 'Unathorized user' };
    }

    const userData = tokenService.validateRefreshToken(refreshToken);
    const tokenFromDb = await tokenService.findToken(refreshToken);

    if (!userData || !tokenFromDb) {
      throw { status: 401, message: 'Unathorized user' };
    }

    const user = (
      await sequelize.model('User').findOne({
        where: {
          id: userData.id,
        },
      })
    )?.get({ plain: true });
    const tokens = tokenService.generateTokens(user);
    await tokenService.saveToken(user.id, tokens.refreshToken);

    return {
      ...tokens,
      user: { id: user.id, email: user.email },
    };
  };
}

export default new AuthService();
