import sequelize from '../database/db.config';
import { Op } from 'sequelize';
import { IChatService } from './interfaces/chat.interface';

class ChatService implements IChatService {
  getChats: IChatService['getChats'] = async (id) => {
    const chats = await sequelize.model('Chat').findAll({
      where: {
        type: 'private',
        members: { [Op.like]: `%${id}%` },
      },
    });

    if (!chats) {
      return [];
    }

    return chats;
  };

  addChat: IChatService['addChat'] = async (members) => {
    const newChat = sequelize.model('Chat').create({
      members,
    });

    return newChat;
  };

  getGroupChats: IChatService['getGroupChats'] = async (id) => {
    const chats = await sequelize.model('Chat').findAll({
      where: {
        type: 'group',
        members: { [Op.like]: `%${id}%` },
      },
    });

    if (!chats) {
      return [];
    }

    return chats;
  };

  addGroupChat: IChatService['addGroupChat'] = async (members, name) => {
    const newChat = sequelize.model('Chat').create({
      type: 'group',
      members,
      name,
    });

    return newChat;
  };
}

export default new ChatService();
