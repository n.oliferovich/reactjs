import jwt from 'jsonwebtoken';
import sequelize from '../database/db.config';
import dotenv from 'dotenv';
import { ITokenService, RefreshType } from './interfaces/token.interface';
dotenv.config();

class TokenService implements ITokenService {
  generateTokens: ITokenService['generateTokens'] = (payload) => {
    const accessToken = jwt.sign(payload, process.env.ACCESS_SECRET as string, {
      expiresIn: '15d',
    });
    const refreshToken = jwt.sign(
      payload,
      process.env.REFRESH_SECRET as string,
      {
        expiresIn: '30d',
      }
    );
    return { accessToken, refreshToken };
  };

  validateAccessToken: ITokenService['validateAccessToken'] = (token) => {
    try {
      const userData = jwt.verify(token, process.env.ACCESS_SECRET as string);

      return userData;
    } catch (e) {
      return null;
    }
  };

  validateRefreshToken: ITokenService['validateRefreshToken'] = (token) => {
    try {
      const userData = jwt.verify(token, process.env.REFRESH_SECRET as string);

      return userData as RefreshType;
    } catch (e) {
      return null;
    }
  };

  saveToken: ITokenService['saveToken'] = async (userId, refreshToken) => {
    const tokenData = await sequelize.model('Token').findOne({
      where: {
        UserId: userId,
      },
    });

    if (tokenData) {
      return await sequelize.model('Token').update(
        {
          refreshToken,
        },
        {
          where: {
            UserId: userId,
          },
        }
      );
    }

    const token = await sequelize.model('Token').create({
      UserId: userId,
      refreshToken,
    });

    return token;
  };

  removeToken: ITokenService['removeToken'] = async (refreshToken) => {
    const tokenData = sequelize
      .model('Token')
      .destroy({ where: { refreshToken } });

    return tokenData;
  };

  findToken: ITokenService['findToken'] = async (refreshToken) => {
    const tokenData = sequelize
      .model('Token')
      .findOne({ where: { refreshToken } });

    return tokenData;
  };
}

export default new TokenService();
