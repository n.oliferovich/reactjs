import sequelize from '../database/db.config';
import { Op } from 'sequelize';
import { IGroupService } from './interfaces/group.interface';

class GroupService implements IGroupService {
  getChats: IGroupService['getChats'] = async (id) => {
    let chats = await sequelize.model('Group').findAll({
      where: {
        members: { [Op.like]: `%${id}%` },
      },
    });

    if (!chats) {
      chats = [];
    }

    return chats;
  };

  addChat: IGroupService['addChat'] = async (members, name) => {
    const newChat = sequelize.model('Group').create({
      members,
      name,
    });

    return newChat;
  };
}

export default new GroupService();
