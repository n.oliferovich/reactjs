import { BrowserRouter as Router } from 'react-router-dom';
import { useRoutes } from './routes';
import { useAuth } from './hooks/auth.hook';
import { AuthContext } from './context/AuthContext';
import Footer from './components/Footer';
import Header from './components/Header';
import './App.css';

function App() {
  const { token, login, logout, userId } = useAuth();
  const isAuthenticated = !!token;
  const routes = useRoutes(isAuthenticated);

  return (
    <AuthContext.Provider value={{ login, logout, userId, isAuthenticated }}>
      <Router>
        <div className='App'>
          <header>{isAuthenticated && <Header />}</header>
          <main>
            <div className='container'>{routes}</div>
          </main>
          <footer className='page-footer blue darken-1'>
            <Footer />
          </footer>
        </div>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
