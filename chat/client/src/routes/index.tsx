import { Routes, Route, Navigate } from 'react-router-dom';
import ChatPage from '../pages/ChatPage';
import LoginPage from '../pages/LoginPage';

export function useRoutes(authenticated: boolean) {
  if (authenticated) {
    return (
      <Routes>
        <Route path='/' element={<ChatPage />} />
        <Route path='*' element={<Navigate to='/' />} />
      </Routes>
    );
  } else {
    return (
      <Routes>
        <Route path='/login' element={<LoginPage />} />
        <Route path='*' element={<Navigate to='/login' />} />
      </Routes>
    );
  }
}
