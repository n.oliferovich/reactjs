import { createContext } from 'react';

export const AuthContext = createContext({
  userId: '',
  isAuthenticated: false,
  login: ({ jwtToken, id }: { jwtToken: string; id: string }) => {},
  logout: () => {},
});
