import React, { useContext, useEffect, useRef, useState } from 'react';
import Message from '../components/Message';
import Modal from '../components/Modal';
import { AuthContext } from '../context/AuthContext';
import { useChat } from '../hooks/chat.hook';
import {
  addChat,
  addMessage,
  getChats,
  getGroups,
  getMessages,
  getUser,
  getUsers,
} from '../http/controllers/chat';
import { User } from '../http/responses/AuthResponse';
import { ChatResponse, MessageResponse } from '../http/responses/ChatResponse';
import 'emoji-mart/css/emoji-mart.css';
import { Picker } from 'emoji-mart';
import './ChatPage.css';
import { Emoji } from 'emoji-mart/dist-es/utils/data';
import Notification from '../components/Notification';

const ChatPage = () => {
  const auth = useContext(AuthContext);
  const userId = auth.userId;
  const messagesRef = useRef<HTMLDivElement>(null);
  const {
    messages,
    sendMessage,
    handleSetMessages,
    currentChat,
    handleSetCurrentChat,
    notification,
    handleSetNotification,
  } = useChat();

  const [messageValue, setMessageValue] = useState('');
  const [chats, setChats] = useState<Array<ChatResponse>>([]);
  const [newChats, setNewChats] = useState<Array<User>>([]);
  const [chatUsers, setChatUsers] = useState<Array<User>>([]);
  const [profile, setProfile] = useState<User>();
  const [groups, setGroups] = useState<Array<ChatResponse>>([]);
  const [emojiVisible, setEmojiVisible] = useState(false);

  const selectChat = (chat: ChatResponse) => {
    if (currentChat?.id !== chat.id) {
      handleSetCurrentChat(chat);
      getMessages(chat.id).then((res) => {
        handleSetMessages(res.data || []);
      });
      const newNotification = [...notification].map((not) => {
        if (not.id === chat.id) {
          not.number = 0;
        }
        return not;
      });
      handleSetNotification([...newNotification]);
    }
  };

  const onSendMessage = (e: React.FormEvent) => {
    e.preventDefault();
    if (messageValue) {
      const receiverId = currentChat?.members.filter(
        (member: string) => member !== userId
      );
      if (receiverId) {
        sendMessage(
          userId,
          [...receiverId],
          currentChat?.id || '',
          messageValue
        );
      }
      if (currentChat) {
        addMessage({
          text: messageValue,
          sender: userId,
          ChatId: currentChat?.id,
        }).then((res) => {
          handleSetMessages([...messages, res.data]);
        });
      }
      setMessageValue('');
    }
  };

  const addNewChat = (receiverId: string) => {
    addChat({ senderId: userId, receiverId }).then((res) => {
      setChats([...chats, res.data]);
      handleSetCurrentChat(res.data);
    });
    getUser(receiverId).then((res) => {
      setChatUsers([...chatUsers, res.data]);
    });
  };

  const setNewGroup = (group: ChatResponse) => {
    setGroups([...groups, group]);
  };

  const selectEmoji = (emoji: Emoji) => {
    const emj = String.fromCodePoint(Number(`0x${emoji.unified}`));
    setMessageValue(messageValue + emj);
    setEmojiVisible(false);
  };

  useEffect(() => {
    getUser(userId).then((res) => {
      setProfile(res.data);
    });
  }, [userId]);

  useEffect(() => {
    getGroups(userId).then((res) => {
      setGroups(res.data.chats);
    });
  }, [userId]);

  useEffect(() => {
    getChats(userId).then((res) => {
      setChats(res.data.chats);
      setChatUsers(res.data.users);
    });
  }, [userId]);

  useEffect(() => {
    messagesRef.current?.scrollTo(0, 999999);
  }, [messages]);

  useEffect(() => {
    let members: string[] = [userId];
    chats?.forEach((chat: ChatResponse) =>
      chat.members?.forEach((member: string) => {
        if (!members.includes(member) && member !== userId) {
          members.push(member);
        }
      })
    );
    getUsers(members).then((res) => {
      setNewChats(res.data);
    });
  }, [chats, userId]);

  return (
    <div className='chat row'>
      <div className='chat-users col s12 m3'>
        <p>
          <b>Hello, {profile?.email}</b>
        </p>
        <Modal friends={chatUsers} users={newChats} setGroup={setNewGroup} />
        <br />
        <b>My Groups ({groups.length})</b>
        <div className='chats mb-1'>
          {groups?.map((group: ChatResponse) => (
            <div
              className={`chatUser ${
                group.id === currentChat?.id ? 'active' : ''
              }`}
              onClick={() => selectChat(group)}
              key={group.id}
            >
              <p>{group.name}</p>
              <Notification notifications={notification} chatId={group.id} />
            </div>
          ))}
        </div>
        <br />
        <b>My Chats ({chats.length})</b>
        <div className='chats mb-1'>
          {chats?.map((chat: ChatResponse) => (
            <div
              className={`chatUser ${
                chat.id === currentChat?.id ? 'active' : ''
              }`}
              onClick={() => selectChat(chat)}
              key={chat.id}
            >
              <p>
                {
                  chatUsers?.find(
                    (user: User) =>
                      user.id ===
                      chat.members?.find((member: string) => member !== userId)
                  )?.email
                }
                <Notification notifications={notification} chatId={chat.id} />
              </p>
            </div>
          ))}
        </div>
        <b>Others ({newChats.length})</b>
        <div className='chats'>
          {newChats?.map((user: User) => (
            <div
              className='chatUser'
              onClick={() => addNewChat(user.id)}
              key={user.id}
            >
              <p>{user.email}</p>
            </div>
          ))}
        </div>
      </div>
      <div className='chat-messages col s12 m9'>
        {currentChat ? (
          <>
            <div className='messages' ref={messagesRef}>
              {messages.length ? (
                messages.map((message: MessageResponse) => (
                  <Message message={message} key={String(message.createdAt)} />
                ))
              ) : (
                <p className='no-messages'>No messages</p>
              )}
            </div>
            <br />
            <hr />
            <div className='chatForm'>
              <div className='row'>
                <div className='col s3 m2'>
                  <div className='chat-smile'>
                    {emojiVisible && (
                      <div className='chat-smile-picker'>
                        <Picker set='apple' onSelect={selectEmoji} />
                      </div>
                    )}
                    <button
                      className='btn-emoji'
                      onClick={(e) => setEmojiVisible(!emojiVisible)}
                    >
                      <i className='material-icons'>add</i>
                    </button>
                  </div>

                  <div className='chat-actions'>
                    <div className='action-item'></div>
                  </div>
                </div>
                <div className='col s9 m10'>
                  <form onSubmit={onSendMessage}>
                    <textarea
                      id='textarea1'
                      className='materialize-textarea form-control'
                      value={messageValue}
                      onChange={(e) => setMessageValue(e.target.value)}
                    ></textarea>
                    <button
                      className='btn btn-primary teal lighten-2 btn-sendmess'
                      onClick={onSendMessage}
                    >
                      Send
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </>
        ) : (
          <h1 className='choose'>Choose a chat</h1>
        )}
      </div>
    </div>
  );
};

export default ChatPage;
