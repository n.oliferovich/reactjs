import React, { FC, useState, useEffect, useContext } from 'react';
import { AuthContext } from '../context/AuthContext';
import { useLocalStorage } from '../hooks/local.hook';
import { useMessage } from '../hooks/message.hook';
import M from 'materialize-css';
import AuthService from '../http/services/AuthService';

const LoginPage: FC = () => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const auth = useContext(AuthContext);
  const message = useMessage();
  const { loading, loginRequest, error, clearError } = useLocalStorage();

  const handleLogin = async (e: React.FormEvent) => {
    e.preventDefault();
    const data = loginRequest({ email, password });
    if (error.message) {
      message(error.message);
    }
    if (data) {
      try {
        const res = await AuthService.login(email, password);
        auth.login({ jwtToken: res.data.accessToken, id: res.data.user.id });
      } catch (err: any) {
        message(err.response.data.error.message);
      }
    }
  };

  const handleRegister = async (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault();
    const data = loginRequest({ email, password });
    if (error.message) {
      message(error.message);
    }
    if (data) {
      try {
        const res = await AuthService.register(email, password);
        auth.login({ jwtToken: res.data.accessToken, id: res.data.user.id });
      } catch (err: any) {
        message(err.response.data.error.message);
      }
    }
  };

  useEffect(() => {
    message(error.message);
    clearError();
  }, [error.message, message, clearError]);

  useEffect(() => {
    M.updateTextFields();
  }, []);

  return (
    <div className='login'>
      <div className='row'>
        <div className='col s6 offset-s3'>
          <h1>Authorization</h1>
          <form className='loginForm card blue darken-1'>
            <div className='card-content white-text'>
              <div>
                <div className='input-field'>
                  <input
                    placeholder='Input Email'
                    id='email'
                    type='text'
                    name='email'
                    className='yellow-input'
                    onChange={(e) => setEmail(e.target.value)}
                    value={email}
                  />
                  <label className='active' htmlFor='email'>
                    Email
                  </label>
                </div>
                <div className='input-field'>
                  <input
                    placeholder='Input Password'
                    id='password'
                    type='password'
                    name='password'
                    className='yellow-input'
                    onChange={(e) => setPassword(e.target.value)}
                    value={password}
                  />
                  <label className='active' htmlFor='password'>
                    Password
                  </label>
                </div>
              </div>
            </div>
            <div className='card-action'>
              <button
                className='btn yellow darken-4'
                style={{ marginRight: 10 }}
                onClick={handleLogin}
                disabled={loading}
              >
                Login
              </button>
              <button
                className='btn grey lighten-1 black-text'
                onClick={handleRegister}
                disabled={loading}
              >
                Registration
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
