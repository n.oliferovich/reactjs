import { useCallback, useContext, useEffect, useRef, useState } from 'react';
import { io, Socket } from 'socket.io-client';
import { AuthContext } from '../context/AuthContext';
import { useMessage } from '../hooks/message.hook';
import { ChatResponse, MessageResponse } from '../http/responses/ChatResponse';
const SERVER_URL = 'http://localhost:5100';

type UserType = {
  userId: string;
  socketId: string;
};

export type NotificationType = {
  number: number;
  id: string;
};

export const useChat = () => {
  const auth = useContext(AuthContext);
  const userId = auth.userId;
  const [users, setUsers] = useState<Array<UserType>>([]);
  const [messages, setMessages] = useState<Array<MessageResponse>>([]);
  const [currentChat, setCurrentChat] = useState<ChatResponse>();
  const socketRef = useRef<Socket>();
  const [notification, setNotification] = useState<Array<NotificationType>>([]);
  const message = useMessage();

  const handleSetMessages = useCallback((message: MessageResponse[]) => {
    setMessages(message);
  }, []);

  const handleSetCurrentChat = useCallback((chat: ChatResponse) => {
    setCurrentChat(chat);
  }, []);

  const handleSetNotification = useCallback(
    (notification: NotificationType[]) => {
      setNotification(notification);
    },
    []
  );

  useEffect(() => {
    socketRef.current = io(SERVER_URL);

    socketRef.current.emit('addUser', userId);

    socketRef.current.on('getUsers', (users: UserType[]) => {
      setUsers(users);
      console.log(users);
    });

    return () => {
      socketRef.current?.disconnect();
    };
  }, [userId]);

  useEffect(() => {
    socketRef.current?.on('getMessage', (data: MessageResponse) => {
      if (data.chatId === currentChat?.id) {
        setMessages([...messages, data]);
      } else {
        let newNotification = {
          id: data.chatId,
          number: 1,
        };
        notification.forEach((not) => {
          if (not.id === data.chatId) {
            newNotification.number = ++not.number;
          }
        });
        setNotification([...notification, newNotification]);
        message(data.text);
      }
    });
  }, [currentChat, messages, notification]);

  const sendMessage = (
    senderId: string,
    receiverId: string[],
    chatId: string,
    text: string
  ) => {
    socketRef.current?.emit('sendMessage', {
      senderId,
      receiverId,
      chatId,
      text,
    });
  };

  return {
    users,
    messages,
    sendMessage,
    handleSetMessages,
    currentChat,
    handleSetCurrentChat,
    notification,
    handleSetNotification,
  };
};
