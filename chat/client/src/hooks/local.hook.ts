import { useState, useCallback } from 'react';

export const useLocalStorage = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState({ message: '' });

  const loginRequest = useCallback(
    (body: { email: string; password: string }) => {
      setLoading(true);
      const user = body;
      if (!user.email || !user.password) {
        setError({ message: 'Input data' });
        setLoading(false);
        return false;
      }

      setLoading(false);
      return true;
    },
    []
  );

  const deleteRequest = useCallback((body: { email: null | string }) => {
    setLoading(true);
    const user = body;

    if (user.email) localStorage.removeItem(user.email);

    setLoading(false);
    return { userLogin: user.email };
  }, []);

  const clearError = useCallback(() => setError({ message: '' }), []);

  return {
    loading,
    loginRequest,
    deleteRequest,
    error,
    clearError,
  };
};
