import { useState, useCallback, useEffect } from 'react';

const storageName = 'userData';

export const useAuth = () => {
  const [token, setToken] = useState('');
  const [userId, setUserId] = useState('');

  const login = useCallback(
    ({ jwtToken, id }: { jwtToken: string; id: string }) => {
      setToken(jwtToken);
      setUserId(id);

      localStorage.setItem(
        storageName,
        JSON.stringify({
          userId: id,
          token: jwtToken,
        })
      );
    },
    []
  );

  const logout = useCallback(() => {
    setToken('');
    setUserId('');
    localStorage.removeItem(storageName);
  }, []);

  useEffect(() => {
    const name = localStorage.getItem(storageName);
    if (name) {
      const data = JSON.parse(name);

      if (data && data.token) {
        login({ jwtToken: data.token, id: data.userId });
      }
    }
  }, [login]);

  return { login, logout, token, userId };
};
