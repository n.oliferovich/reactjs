import {
  FormEvent,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { AuthContext } from '../context/AuthContext';
import { User } from '../http/responses/AuthResponse';
import GroupService from '../http/services/GroupService';
import './Modal.css';

const Modal = ({
  friends,
  users,
  setGroup,
}: {
  friends: User[];
  users: User[];
  setGroup: Function;
}) => {
  const auth = useContext(AuthContext);
  const userId = auth.userId;
  const modal = useRef(null);
  const [name, setName] = useState('');
  const [selectedUsers, setSelectedUsers] = useState<Set<string>>(new Set());

  const handleCheck = (value: string) => {
    setSelectedUsers((prev) => {
      if (prev.has(value)) {
        selectedUsers.delete(value);
        return new Set(selectedUsers);
      } else {
        return new Set(selectedUsers.add(value));
      }
    });
  };

  async function addGroup(members: Set<string>, name: string) {
    return await GroupService.addChat(members, name);
  }

  const handleSubmit = useCallback(
    async (e: FormEvent) => {
      e.preventDefault();
      const res = await addGroup(selectedUsers.add(userId), name || 'No name');
      setGroup(res.data);
      setName('');
      setSelectedUsers(new Set());
      if (modal.current) M.Modal.init(modal.current).close();
    },
    [name, selectedUsers, setGroup, userId]
  );

  const userItem = (user: User) => {
    return (
      <p key={user.id}>
        <label>
          <input
            type='checkbox'
            className='filled-in'
            value={user.id}
            onChange={(e) => handleCheck(e.target.value)}
          />
          <span>{user.email}</span>
        </label>
      </p>
    );
  };

  const friendsArr = useMemo(() => {
    return friends?.map((friend) => {
      return userItem(friend);
    });
  }, [friends]);

  const usersArr = useMemo(() => {
    return users?.map((user) => {
      return userItem(user);
    });
  }, [users]);

  useEffect(() => {
    if (modal.current) M.Modal.init(modal.current);
  }, []);

  return (
    <div className='add-group-btn'>
      <a
        className='waves-effect waves-light btn modal-trigger'
        href='#modalGroup'
      >
        Add group
      </a>
      <div id='modalGroup' className='modal' ref={modal}>
        <div className='modal-content'>
          <div className='modal-header'>
            <h4>New group</h4>
          </div>
          <div className='modal-body'>
            <form className='groupForm' onSubmit={handleSubmit}>
              <div className='input-field'>
                <input
                  id='name'
                  type='text'
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
                <label className='active' htmlFor='name'>
                  Group Name
                </label>
              </div>
              <div className='input-field'>
                <p className='input-title'>Choose room users</p>
                <p>My friends</p>
                {friendsArr}
                <p>Other users</p>
                {usersArr}
              </div>
            </form>
          </div>
        </div>
        <div className='modal-footer'>
          <button className='modal-close waves-effect waves-green btn-flat'>
            Cancel
          </button>
          <button
            type='submit'
            className='waves-effect waves-green waves-light btn'
            onClick={handleSubmit}
          >
            Create
          </button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
