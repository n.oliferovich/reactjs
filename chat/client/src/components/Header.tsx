import { MouseEvent, useContext } from 'react';
import { AuthContext } from '../context/AuthContext';

const Header = () => {
  const auth = useContext(AuthContext);

  const logoutHandler = (event: MouseEvent) => {
    event.preventDefault();
    auth.logout();
  };

  return (
    <nav>
      <div className='nav-wrapper blue darken-1' style={{ padding: '0 2rem' }}>
        <span className='brand-logo'>VironIt</span>
        <ul id='nav-mobile' className='right'>
          <li>
            <p onClick={logoutHandler}>Logout</p>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Header;
