import { NotificationType } from '../hooks/chat.hook';

const Notification = ({
  notifications,
  chatId,
}: {
  notifications: NotificationType[];
  chatId: string;
}) => {
  let number;

  notifications.forEach((not) => {
    if (not.id === chatId && not.number > 0)
      number = <span className='not'>{not.number}</span>;
  });

  return <span>{number || ''}</span>;
};

export default Notification;
