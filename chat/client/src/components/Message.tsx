import { useContext } from 'react';
import { format } from 'timeago.js';
import { AuthContext } from '../context/AuthContext';
import { MessageResponse } from '../http/responses/ChatResponse';

const Message = ({ message }: { message: MessageResponse }) => {
  const auth = useContext(AuthContext);
  const userId = auth.userId;

  return (
    <div className={`message ${userId === message.sender && 'message-right'}`}>
      <p
        className={`card-panel ${
          userId === message.sender ? 'teal lighten-2' : 'grey lighten-2'
        }`}
      >
        {message.text}
      </p>
      <div>
        <span className='message-author'>{format(message.createdAt)}</span>
      </div>
    </div>
  );
};

export default Message;
