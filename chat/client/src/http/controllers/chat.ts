import ChatService from '../services/ChatService';
import GroupService from '../services/GroupService';
import UserService from '../services/UserService';

export async function getMessages(id: string) {
  return await ChatService.getMessages(id);
}

export async function addMessage({
  sender,
  ChatId,
  text,
}: {
  sender: string;
  ChatId: string;
  text: string;
}) {
  return await ChatService.addMessage(sender, ChatId, text);
}

export async function getChats(userId: string) {
  return await ChatService.getChats(userId);
}

export async function addChat({
  senderId,
  receiverId,
}: {
  senderId: string;
  receiverId: string;
}) {
  return await ChatService.addChat(senderId, receiverId);
}

export async function getUsers(usersIds: string[]) {
  return await UserService.getUsers(usersIds);
}

export async function getUser(id: string) {
  return await UserService.getUser(id);
}

export async function getGroups(userId: string) {
  return await GroupService.getChats(userId);
}
