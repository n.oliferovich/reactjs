import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { AuthResponse } from './responses/AuthResponse';

export const API_URL = 'http://localhost:5000/api';

const api = axios.create({
  withCredentials: true,
  baseURL: API_URL,
});

api.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    const data = localStorage.getItem('userData');
    if (data) {
      const userData = JSON.parse(data);
      (config.headers ??= {}).Authorization = `Bearer ${userData.token}`;
    }

    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

api.interceptors.response.use(
  (config: AxiosResponse) => {
    return config;
  },
  async (error) => {
    const originalRequest = error.config;

    if (
      error.response.status === 401 &&
      error.config &&
      !error.config._isRetry
    ) {
      originalRequest._isRetry = true;
      try {
        const response = await axios.get<AuthResponse>(
          `${API_URL}/auth/refresh`,
          { withCredentials: true }
        );
        localStorage.setItem('token', response.data.accessToken);

        return api.request(originalRequest);
      } catch (error) {
        return Promise.reject(error);
      }
    }

    throw error;
  }
);

export default api;
