import { User } from './AuthResponse';

export interface MessageResponse {
  sender: string;
  text: string;
  chatId: string;
  createdAt: Date;
}

export interface ChatResponse {
  id: string;
  members: string[];
  name: string;
  type: string;
  date: Date;
  UserId: string;
}

export interface ChatsUsersResponse {
  chats: ChatResponse[];
  users: User[];
}
