import { AxiosResponse } from 'axios';
import api from '..';
import { User } from '../responses/AuthResponse';

export default class UserService {
  static async getUsers(
    usersIds: string[]
  ): Promise<AxiosResponse<Array<User>>> {
    return api.post<Array<User>>(`/users/`, { usersIds });
  }

  static async getUser(id: string): Promise<AxiosResponse<User>> {
    return api.get<User>(`/users/user/${id}`);
  }
}
