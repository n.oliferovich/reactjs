import { AxiosResponse } from 'axios';
import api from '..';
import { ChatsUsersResponse, ChatResponse } from '../responses/ChatResponse';

export default class GroupService {
  static async getChats(
    id: string
  ): Promise<AxiosResponse<ChatsUsersResponse>> {
    return api.get<ChatsUsersResponse>(`/group/${id}`);
  }

  static async addChat(
    members: Set<string>,
    name: string
  ): Promise<AxiosResponse<ChatResponse>> {
    return api.post<ChatResponse>(`/group/`, {
      members: Array.from(members),
      name,
    });
  }
}
