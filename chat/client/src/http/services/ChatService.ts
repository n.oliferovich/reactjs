import { AxiosResponse } from 'axios';
import api from '..';
import {
  ChatResponse,
  ChatsUsersResponse,
  MessageResponse,
} from '../responses/ChatResponse';

export default class ChatService {
  static async getChats(
    id: string
  ): Promise<AxiosResponse<ChatsUsersResponse>> {
    return api.get<ChatsUsersResponse>(`/chats/${id}`);
  }

  static async addChat(
    senderId: string,
    receiverId: string
  ): Promise<AxiosResponse<ChatResponse>> {
    return api.post<ChatResponse>(`/chats/`, { senderId, receiverId });
  }

  static async getMessages(
    id: string
  ): Promise<AxiosResponse<Array<MessageResponse>>> {
    return api.get<Array<MessageResponse>>(`/messages/${id}`);
  }

  static async addMessage(
    sender: string,
    ChatId: string,
    text: string
  ): Promise<AxiosResponse<MessageResponse>> {
    return api.post<MessageResponse>(`/messages/`, { sender, ChatId, text });
  }
}
