import { AxiosResponse } from 'axios';
import api from '..';
import { AuthResponse } from '../responses/AuthResponse';

export default class AuthService {
  static async register(
    email: string,
    password: string
  ): Promise<AxiosResponse<AuthResponse>> {
    return api.post<AuthResponse>('/auth/registration', { email, password });
  }

  static async login(
    email: string,
    password: string
  ): Promise<AxiosResponse<AuthResponse>> {
    return api.post<AuthResponse>('/auth/login', { email, password });
  }
}
