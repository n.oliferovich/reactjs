import { Request, Response } from 'express';
import { validationResult } from 'express-validator';
import authService from '../services/auth.service';
import { catchStatusError, catchTextError } from './error.controller';

const days = 30;
const hours = 24;
const minutes = 60;
const seconds = 60;
const milliseconds = 1000;

class AuthController {
  async register(req: Request, res: Response) {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ error: { message: `Validation error` } });
      }
      const { email, password } = req.body;
      const userData = await authService.register(email, password);
      res.cookie('refreshToken', userData.refreshToken, {
        maxAge: days * hours * minutes * seconds * milliseconds,
        httpOnly: true,
      });

      return res.json(userData);
    } catch (err: any) {
      res.status(catchStatusError(err)).json(catchTextError(err));
    }
  }

  async login(req: Request, res: Response) {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ error: { message: `Validation error` } });
      }

      const { email, password } = req.body;
      const userData = await authService.login(email, password);

      res.cookie('refreshToken', userData.refreshToken, {
        maxAge: days * hours * minutes * seconds * milliseconds,
        httpOnly: true,
      });

      return res.json(userData);
    } catch (err: any) {
      res.status(catchStatusError(err)).json(catchTextError(err));
    }
  }

  async logout(req: Request, res: Response) {
    try {
      const { refreshToken } = req.cookies;
      const token = authService.logout(refreshToken);
      res.clearCookie('refreshToken');

      return res.json(token);
    } catch (err: any) {
      res.status(catchStatusError(err)).json(catchTextError(err));
    }
  }

  async refresh(req: Request, res: Response) {
    try {
      const { refreshToken } = req.cookies;
      const userData = await authService.refresh(refreshToken);
      res.cookie('refreshToken', userData.refreshToken, {
        maxAge: days * hours * minutes * seconds * milliseconds,
        httpOnly: true,
      });

      return res.json(userData);
    } catch (err: any) {
      res.status(catchStatusError(err)).json(catchTextError(err));
    }
  }
}

export default new AuthController();
