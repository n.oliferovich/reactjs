import express from 'express';
import chatService from '../services/chat.service';
import userService from '../services/user.service';
import { catchStatusError, catchTextError } from './error.controller';

class GroupController {
  async getChats(req: express.Request, res: express.Response) {
    try {
      const id = req.params.id;
      const chats = await chatService.getGroupChats(id);
      const members: string[] = [];

      if (chats.length) {
        chats.forEach((chat: any) =>
          chat.members.forEach((member: string) => {
            if (!members.includes(member) && member !== id) {
              members.push(member);
            }
          })
        );
      }

      const users = await userService.getUsersByIds(members);

      return res.json({ chats, users });
    } catch (err: any) {
      res.status(catchStatusError(err)).json(catchTextError(err));
    }
  }

  async addChat(req: express.Request, res: express.Response) {
    try {
      const members = req.body.members;
      const name = req.body.name;
      const newChat = await chatService.addGroupChat(members, name);

      return res.json(newChat);
    } catch (err: any) {
      res.status(catchStatusError(err)).json(catchTextError(err));
    }
  }
}

export default new GroupController();
