export const catchTextError = (err: any) => {
  return { error: { message: err.message || 'Server error...' } };
};

export const catchStatusError = (err: any) => {
  return err.status || 500;
};
