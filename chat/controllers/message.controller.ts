import express from 'express';
import messageService from '../services/message.service';
import { catchStatusError, catchTextError } from './error.controller';

class MessageController {
  async getMessages(req: express.Request, res: express.Response) {
    try {
      const id = req.params.id;
      const messages = await messageService.getMessages(id);

      return res.json(messages);
    } catch (err: any) {
      res.status(catchStatusError(err)).json(catchTextError(err));
    }
  }

  async addMessage(req: express.Request, res: express.Response) {
    try {
      const sender = req.body.sender;
      const ChatId = req.body.ChatId;
      const text = req.body.text;
      const message = await messageService.addMessage(sender, ChatId, text);

      return res.json(message);
    } catch (err: any) {
      res.status(catchStatusError(err)).json(catchTextError(err));
    }
  }
}

export default new MessageController();
