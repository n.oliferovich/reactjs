import { Request, Response } from 'express';
import userService from '../services/user.service';
import { catchStatusError, catchTextError } from './error.controller';

class UserController {
  async getUsersExcludeId(req: Request, res: Response) {
    try {
      const id = req.params.id;
      const users = await userService.getUsersExcludeId(id);

      return res.json(users);
    } catch (err: any) {
      res.status(catchStatusError(err)).json(catchTextError(err));
    }
  }

  async getUser(req: Request, res: Response) {
    try {
      const id = req.params.id;
      const user = await userService.getUser(id);

      return res.json(user);
    } catch (err: any) {
      res.status(catchStatusError(err)).json(catchTextError(err));
    }
  }

  async getUsersExcludeIds(req: Request, res: Response) {
    try {
      const ids = req.body.usersIds;
      const users = await userService.getUsersExcludeIds(ids);

      return res.json(users);
    } catch (err: any) {
      res.status(catchStatusError(err)).json(catchTextError(err));
    }
  }
}

export default new UserController();
