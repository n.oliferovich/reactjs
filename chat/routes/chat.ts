import { Router } from 'express';
import { default as chatController } from '../controllers/chat.controller';
import { authMiddleware } from '../middlewares/auth.middleware';
export const chatRouter = Router();

chatRouter.get('/:id', authMiddleware, chatController.getChats);
chatRouter.post('/', chatController.addChat);
