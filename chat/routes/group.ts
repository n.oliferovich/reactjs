import { Router } from 'express';
import { default as groupController } from '../controllers/group.controller';
export const groupRouter = Router();

groupRouter.get('/:id', groupController.getChats);
groupRouter.post('/', groupController.addChat);
