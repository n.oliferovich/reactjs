import { Router } from 'express';
import { default as userController } from '../controllers/user.controller';

export const userRouter = Router();

userRouter.get('/:id', userController.getUsersExcludeId);
userRouter.get('/user/:id', userController.getUser);
userRouter.post('/', userController.getUsersExcludeIds);
