import { Router } from 'express';
import { authRouter } from './auth';
import { userRouter } from './user';
import { chatRouter } from './chat';
import { messageRouter } from './message';
import { groupRouter } from './group';

export const router = Router();

router.use('/auth', authRouter);
router.use('/users', userRouter);
router.use('/chats', chatRouter);
router.use('/messages', messageRouter);
router.use('/group', groupRouter);
