import { Request, Response, NextFunction } from 'express';
import tokenService from '../services/token.service';

export interface AuthRequest extends Request {
  user?: any;
}

export function authMiddleware(
  req: AuthRequest,
  res: Response,
  next: NextFunction
) {
  try {
    const authorizationHeader = req.headers.authorization;

    if (!authorizationHeader) {
      return res.status(403).send({
        error: { message: 'No token provided!' },
      });
    }

    const accessToken = authorizationHeader.split(' ')[1];

    if (!accessToken) {
      return res.status(401).send({
        error: { message: 'Unauthorized!' },
      });
    }

    const userData = tokenService.validateAccessToken(accessToken);

    if (!userData) {
      return res.status(401).send({
        error: { message: 'Unauthorized!' },
      });
    }

    req.user = userData;
    next();
  } catch (e) {
    return next({
      error: { message: 'Unauthorized!' },
    });
  }
}
