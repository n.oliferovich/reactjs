import { Server, Socket } from 'socket.io';

type UsersType = {
  userId: string;
  socketId: string;
};

const initSockets = () => {
  const io = new Server(5100, { cors: { origin: 'http://localhost:3000' } });

  const users: UsersType[] = [];

  const addUser = (userId: string, socketId: string) => {
    !users.some((user) => user.userId === userId) &&
      users.push({ userId, socketId });
  };

  const removeUser = (socketId: string) => {
    users.filter((user) => user.socketId !== socketId);
  };

  const getUser = (userId: string) => {
    return users.find((user) => user.userId === userId);
  };

  io.on('connection', (socket: Socket) => {
    console.log('User connected: ' + socket.id);

    socket.on('addUser', (userId: string) => {
      addUser(userId, socket.id);
      io.emit('getUsers', users);
    });

    socket.on(
      'sendMessage',
      ({
        senderId,
        receiverId,
        chatId,
        text,
      }: {
        senderId: string;
        receiverId: string[];
        chatId: string;
        text: string;
      }) => {
        receiverId.forEach((receiver) => {
          const user = getUser(receiver);
          if (user) {
            io.to(user.socketId).emit('getMessage', {
              sender: senderId,
              text,
              chatId,
              createdAt: new Date(),
            });
          }
        });
      }
    );

    socket.on('disconnect', () => {
      console.log('User disconnected');
      removeUser(socket.id);
      io.emit('getUsers', users);
    });
  });
};

export default initSockets;
