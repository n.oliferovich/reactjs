import { Model } from 'sequelize/types';

export interface UserResponse extends Model {
  id: string;
  email: string;
  password: string;
}

export interface TokenResponse extends Model {
  refreshToken: string;
}

export interface ChatResponse extends Model {
  id: string;
  members: string[];
  name: string | null;
  type: string;
}

export interface GroupResponse extends Model {
  id: string;
  members: string[];
  name: string;
}

export interface MessageResponse extends Model {
  id: string;
  sender: string;
  text: string;
}

export type ChatType = {
  id: string;
  members: string[];
  name: string | null;
  type: string;
};
