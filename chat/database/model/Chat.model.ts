import sequelize from '../db.config';
import pkg from 'sequelize';
import { ChatResponse } from '../response';
const { DataTypes } = pkg;

const Chat = sequelize.define<ChatResponse>('Chat', {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4,
  },
  members: {
    type: DataTypes.STRING,
    allowNull: false,
    get() {
      return this.getDataValue('members').split(';');
    },
    set(val: string[]) {
      this.setDataValue('members', val.join(';'));
    },
  },
  name: {
    type: DataTypes.STRING,
  },
  type: {
    type: DataTypes.ENUM('private', 'group'),
    defaultValue: 'private',
  },
});

export default Chat;
