import sequelize from '../db.config';
import pkg from 'sequelize';
import { MessageResponse } from '../response';
const { DataTypes } = pkg;

const Message = sequelize.define<MessageResponse>('Message', {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4,
  },
  sender: {
    type: DataTypes.STRING,
  },
  text: {
    type: DataTypes.STRING,
  },
});

export default Message;
