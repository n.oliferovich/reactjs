import sequelize from '../db.config';
import pkg from 'sequelize';
import { TokenResponse } from '../response';
const { DataTypes } = pkg;

const Token = sequelize.define<TokenResponse>('Token', {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4,
  },
  refreshToken: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

export default Token;
