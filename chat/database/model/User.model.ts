import sequelize from '../db.config';
import pkg from 'sequelize';
import { UserResponse } from '../response';
const { DataTypes } = pkg;

const User = sequelize.define<UserResponse>('User', {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

export default User;
