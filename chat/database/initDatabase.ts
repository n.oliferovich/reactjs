import sequelize from './db.config';
import { User, Token, Chat, Message } from './model/index';

User.hasOne(Token);
Token.belongsTo(User);

Message.belongsTo(Chat);

export const initDatabase = async () => {
  try {
    await sequelize.sync();

    console.log('MySQL connected');
  } catch (e) {
    console.log('Failed to connect' + (e as Error).message);
    process.exit(1);
  }
};
