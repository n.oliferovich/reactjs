import { Sequelize, Dialect } from 'sequelize';
import dotenv from 'dotenv';
dotenv.config();

const sequelize = new Sequelize(
  process.env.DB as string,
  process.env.USER_DB as string,
  process.env.PASSWORD_DB as string,
  {
    host: process.env.HOST_DB,
    dialect: process.env.DIALECT as Dialect,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  }
);

export default sequelize;
