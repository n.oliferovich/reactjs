import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import { router } from './routes';
import dotenv from 'dotenv';
import { initDatabase } from './database/initDatabase';
import bodyParser from 'body-parser';
dotenv.config();
import initSockets from './sockets';

const app = express();
app.use(
  cors({
    credentials: true,
    origin: 'http://localhost:3000',
  })
);
app.use(express.json());
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.urlencoded({ extended: true }));
app.use('/api', router);

initSockets();

app.listen(process.env.PORT, async () => {
  console.log('Server is running on http://localhost:' + process.env.PORT);

  await initDatabase();
});
