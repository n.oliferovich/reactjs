export interface StateType {
  app: {
    releases: ReleaseResponse[];
    search: SearchResponse;
    currentSong: Track;
    currentAlbum: Album;
    currentArtist: Artist;
    currentArtistTracks: Track[];
    favorites: [];
    recommendations: Track[];
    playingSong: Track;
    playing: boolean;
    loading: boolean;
    alert: string | null;
  };
}

export interface ReleaseResponse {
  artists: ArtistResponse[];
  href: string;
  id: string;
  images: ImageResponse[];
  name: string;
  type: string;
}

export interface ArtistResponse {
  href: string;
  id: string;
  name: string;
}

export interface ImageResponse {
  height: number;
  url: string;
  width: number;
}

export interface NewReleasesResponse {
  albums: {
    herf: string;
    items: ReleaseResponse[];
  };
}

export interface SearchDataResponse {
  data: SearchResponse;
}

export interface ArtistResponseData {
  data: SearchArtistResponse;
}

export interface SearchResponse {
  artists: {
    items: SearchArtistResponse[];
  };
  tracks: {
    items: Track[];
  };
}

export interface SearchArtistResponse extends ArtistResponse {
  popularity: string;
  images: ImageResponse[];
  genres: string[];
  followers: {
    total: number;
  };
}

export interface TrackResponse {
  data: Track;
}

export interface Track {
  album: {
    artists: ArtistResponse[];
    images: ImageResponse[];
  };
  artists: ArtistResponse[];
  href: string;
  id: string;
  name: string;
  popularity: number;
  preview_url: string;
}
export interface AlbumResponse {
  data: Album;
}

export interface Album {
  artists: ArtistResponse[];
  href: string;
  id: string;
  name: string;
  popularity: number;
  label: string;
  images: ImageResponse[];
  tracks: {
    items: Track[];
  };
}

export interface RecommendationResponse {
  data: { tracks: Track[] };
}

export interface FavoritesResponse {
  data: {
    items: Track[];
  };
}

export interface ArtistTopTracks {
  data: { tracks: Track[] };
}

export interface Artist extends ArtistResponse {
  popularity: number;
}
