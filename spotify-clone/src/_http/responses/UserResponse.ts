import { ImageResponse } from "./SpotifyResponse";

export interface UserProfileResponse {
  data: UserProfile;
}

export interface UserProfile {
  country: string;
  email: string;
  id: string;
  display_name: string;
  images: ImageResponse[]
}

export interface UserStateType {
  user: {
    token: string;
    user: UserProfile;
    playlists: any;
    profile: UserProfile;
    alert: null | string;
  };
}
