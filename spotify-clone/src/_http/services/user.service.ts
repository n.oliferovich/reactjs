import { AxiosResponse } from "axios";
import api from "..";
import { SearchResponse } from "../responses/SpotifyResponse";

export default class UserService {

  static async getProfile(): Promise<AxiosResponse<SearchResponse>> {
    return await api.get<SearchResponse>("/me");
  }
}