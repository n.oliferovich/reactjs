import { AxiosResponse } from "axios";
import api from "..";
import {
  AlbumResponse,
  ArtistResponseData,
  NewReleasesResponse,
  RecommendationResponse,
  SearchResponse,
  TrackResponse,
} from "../responses/SpotifyResponse";

export default class SpotifyService {
  static async getNewReleases(): Promise<AxiosResponse<NewReleasesResponse>> {
    return await api.get<NewReleasesResponse>(
      "/browse/new-releases?offset=0&limit=8"
    );
  }

  static async getRecomendations(): Promise<
    AxiosResponse<RecommendationResponse>
  > {
    return await api.get<RecommendationResponse>(
      "recommendations?seed_artists=4NHQUGzhtTLFvgF5SZesLK&seed_genres=classical,country&seed_tracks=0c6xIDDpzE81m2q797ordA&limit=10"
    );
  }

  static async searchSongs(
    title: string
  ): Promise<AxiosResponse<SearchResponse>> {
    return await api.get<SearchResponse>(
      `/search?type=track,artist&q=` + title
    );
  }

  static async getTrack(id: string): Promise<AxiosResponse<TrackResponse>> {
    return await api.get<TrackResponse>("/tracks/" + id);
  }

  static async getAlbum(id: string): Promise<AxiosResponse<AlbumResponse>> {
    return await api.get<AlbumResponse>("/albums/" + id);
  }

  static async getFavorites(): Promise<AxiosResponse<AlbumResponse>> {
    return await api.get<AlbumResponse>("/me/tracks");
  }

  static async getArtistSongs(
    id: string,
    country: string
  ): Promise<AxiosResponse<SearchResponse>> {
    return await api.get<SearchResponse>(
      `/artists/${id}/top-tracks?market=` + country
    );
  }

  static async getArtist(
    id: string
  ): Promise<AxiosResponse<ArtistResponseData>> {
    return api.get<ArtistResponseData>("/artists/" + id);
  }

  static async saveSong(id: string): Promise<AxiosResponse<void>> {
    return api.put<void>("/me/tracks/ids=" + id);
  }

  static async deleteSong(id: string): Promise<AxiosResponse<void>> {
    return api.delete<void>("/me/tracks/ids=" + id);
  }
}
