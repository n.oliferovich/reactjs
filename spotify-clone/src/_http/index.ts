import axios, { AxiosRequestConfig } from 'axios';

export const API_URL = 'https://api.spotify.com/v1';

const api = axios.create({
  baseURL: API_URL,
});

api.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    const accessToken = localStorage.getItem('spotifyAuthToken');
    if (accessToken) {
      (config.headers ??= {}).Authorization = `Bearer ${accessToken}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default api;
