import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { setPlaying, setPlayingSong } from "../../_redux/actions/app.action";
import {
  ArtistResponse,
  StateType,
  Track,
} from "../../_http/responses/SpotifyResponse";
import "./SongItem.css";

const SongItem = ({ song }: { song: Track }) => {
  const dispatch = useDispatch();
  const playing = useSelector((state: StateType) => state.app.playing);
  const playingSong = useSelector((state: StateType) => state.app.playingSong);

  const togglePlay = () => {
    if (!playing && playingSong.id !== song.id) {
      dispatch(setPlayingSong(song));
      dispatch(setPlaying(true));
    } else if (playing && playingSong.id !== song.id) {
      dispatch(setPlayingSong(song));
    } else if (playing && playingSong.id === song.id) {
      dispatch(setPlaying(false));
    } else {
      dispatch(setPlaying(true));
    }
  };

  const toggleFav = () => {};

  return (
    <div className="song">
      <div className="col s10">
        <div className="songRow">
          <span onClick={togglePlay}>
            <i className="material-icons">
              {playing && playingSong.id === song.id
                ? "pause_circle_outline"
                : "play_circle_outline"}
            </i>
          </span>
          <div className="songRow__info">
            <h1>
              <Link to={"/track/" + song.id}>{song.name}</Link>
            </h1>
            <p>
              {song.artists.map((artist: ArtistResponse) => (
                <Link to={"/artist/" + artist.id} key={artist.id}>
                  {artist.name}{" "}
                </Link>
              ))}
              - {song.name}
            </p>
          </div>
        </div>
      </div>
      <div className="col s2">
        <span className="add-fav" onClick={toggleFav}>
          <i className="material-icons">favorite_border</i>
        </span>
      </div>
    </div>
  );
};

export { SongItem };
