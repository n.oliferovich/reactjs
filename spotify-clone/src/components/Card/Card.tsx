import { Link } from "react-router-dom";
import {
  ArtistResponse,
  ReleaseResponse,
} from "../../_http/responses/SpotifyResponse";
import "./Card.css";

const Card = ({ album }: { album: ReleaseResponse }) => {
  return (
    <div className="col s6 m3">
      <div className="card">
        <div className="card-image">
          <img src={album.images[1].url} alt={album.name} />
          <span className="card-title">{album.name}</span>
          <span className="btn-floating halfway-fab waves-effect waves-light red">
            <i className="material-icons">add</i>
          </span>
        </div>
        <div className="card-content">
          <p>Type: {album.type}</p>
          <p className="artists">
            Artist:{" "}
            <span>
              {album.artists.map((artist: ArtistResponse) => (
                <Link to={"/artists/" + artist.id} key={artist.id}>
                  {artist.name + " "}
                </Link>
              ))}
            </span>
          </p>
        </div>
      </div>
    </div>
  );
};

export { Card };
