import { Link } from "react-router-dom";
import {
  ArtistResponse,
  ReleaseResponse,
} from "../../_http/responses/SpotifyResponse";
import "./AlbumItem.css";

const AlbumItem = ({ album }: { album: ReleaseResponse }) => {
  return (
    <div className="col s6 m3">
      <Link to={"/album/" + album.id} className="albumRow">
        <img
          className="albumRow__album"
          src={album.images[0].url}
          alt={album.name}
        />
        <div className="albumRow__info">
          <h1>{album.name}</h1>
          <p>
            {album.artists
              .map((artist: ArtistResponse) => artist.name)
              .join(", ")}{" "}
          </p>
        </div>
      </Link>
    </div>
  );
};

export { AlbumItem };
