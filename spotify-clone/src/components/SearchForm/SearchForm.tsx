import { useState } from "react";
import { useDispatch } from "react-redux";
import { searchSongs } from "../../_redux/actions/app.action";

const SearchForm = () => {
  const dispatch = useDispatch();
  const [title, setTitle] = useState("");

  const changeHandler = (e: string) => {
    if (e.trim()) {
      setTitle(e);
      dispatch(searchSongs(e));
    }
  };

  return (
    <form className="search__form col s12">
      <div className="row">
        <div className="input-field col s12">
          <input
            placeholder="Input song title"
            id="title"
            type="text"
            value={title}
            onChange={(e) => changeHandler(e.target.value)}
          />
          <label className="active" htmlFor="title">
            Title
          </label>
        </div>
      </div>
    </form>
  );
};

export { SearchForm };
