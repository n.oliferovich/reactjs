import { useSelector } from "react-redux";
import { UserStateType } from "../../_http/responses/UserResponse";
import nophoto from "../../assets/img/no_user.png";
import "./User.css";

const User = () => {
  const user = useSelector((state: UserStateType) => state.user.user);

  return (
    <div className="header">
      <div className="header__right">
        <div className="user-avatar">
          <img
            src={user?.images?.length ? user?.images[0]?.url : nophoto}
            alt={user?.display_name}
          />
        </div>
        <h4>{user?.display_name}</h4>
      </div>
    </div>
  );
};

export { User };
