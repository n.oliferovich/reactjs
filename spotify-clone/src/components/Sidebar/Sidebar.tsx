import { useSelector } from "react-redux";
import spotifyLogo from "../../assets/img/Spotify_Logo_RGB_White.png";
import { UserStateType } from "../../_http/responses/UserResponse";
import { SidebarOption } from "./SidebarOption";
import "./Sidebar.css";
import { Link } from "react-router-dom";

const Sidebar = () => {
  const playlists = useSelector((state: UserStateType) => state.user.playlists);

  return (
    <div className="sidebar">
      <Link to="/">
        <img className="sidebar__logo" src={spotifyLogo} alt="Spotify" />
      </Link>

      <SidebarOption icon="home" title="Home" href="/" />
      <SidebarOption icon="search" title="Search" href="/search" />
      <SidebarOption icon="favorite" title="Favorites" href="/favorites" />

      <br />
      <strong className="sidebar__title">PLAYLISTS</strong>
      <hr />

      {playlists?.map((playlist: any) => (
        <SidebarOption
          title={playlist.name}
          icon="playlist_play"
          href={playlist.id}
        />
      ))}
    </div>
  );
};

export { Sidebar };
