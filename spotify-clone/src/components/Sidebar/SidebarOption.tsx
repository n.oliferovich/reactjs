import { Link } from "react-router-dom";

type SidebarType = {
  title: string;
  icon: string;
  href: string;
};

const SidebarOption = ({ title, icon, href }: SidebarType) => {
  return (
    <div>
      <Link to={href} className="sidebarOption">
        {icon && <i className="material-icons">{icon}</i>}
        <h4>{title}</h4>
      </Link>
    </div>
  );
};

export { SidebarOption };
