import SkipPreviousIcon from "@material-ui/icons/SkipPrevious";
import SkipNextIcon from "@material-ui/icons/SkipNext";
import "./Footer.css";
import ReactAudioPlayer from "react-audio-player";
import { useDispatch, useSelector } from "react-redux";
import {
  ArtistResponse,
  StateType,
} from "../../_http/responses/SpotifyResponse";
import { useEffect, useRef } from "react";
import "react-h5-audio-player/lib/styles.css";
import { setPlaying } from "../../_redux/actions/app.action";

const Footer = () => {
  const dispatch = useDispatch();
  const playing = useSelector((state: StateType) => state.app.playing);
  const playingSong = useSelector((state: StateType) => state.app.playingSong);
  const player = useRef<any>(null);

  useEffect(() => {
    if (playing) {
      player.current?.audioEl.play();
    } else {
      player.current?.pause();
    }
  }, [playing]);

  return (
    <div className="footer">
      <div className="footer__left">
        <img
          className="footer__albumLogo"
          src="https://cdn03.ciceksepeti.com/cicek/kcm15551216-1/XL/baris-manco-iste-baris-iste-manco-plak-kcm15551216-1-3d6eb2939c104743b3ae2ebeaf3645d7.jpg"
          alt={playingSong?.name}
        />
        <div className="footer__songInfo">
          <h4>{playingSong?.name}</h4>
          <p>
            {playingSong?.artists?.length &&
              playingSong.artists
                .map((artist: ArtistResponse) => artist.name)
                .join(", ")}{" "}
          </p>
        </div>
      </div>

      <div className="footer__center">
        <SkipPreviousIcon className="footer__icon" />
        <SkipNextIcon className="footer__icon" />
        <ReactAudioPlayer
          controls
          autoPlay={playing}
          src={playingSong?.preview_url}
          ref={(e) => player}
          onPlay={(e) => dispatch(setPlaying(true))}
          onPause={(e) => dispatch(setPlaying(false))}
        />
      </div>

      <div className="footer__right"></div>
    </div>
  );
};

export { Footer };
