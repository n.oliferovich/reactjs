import { useEffect } from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import SpotifyWebApi from "spotify-web-api-js";
import { Home } from "./pages/Home/Home";
import { Search } from "./pages/Search/Search";
import getHash from "./getHash";
import { setPlaylists, setToken, setUser } from "./_redux/actions/user.action";
import { UserStateType } from "./_http/responses/UserResponse";
import { Login } from "./pages/Login/Login";
import { Song } from "./pages/Song/Song";
import { Album } from "./pages/Album/Album";
import { Favorites } from "./pages/Favorites/Favorites";
import { Artist } from "./pages/Artist/Artist";
import "react-spotify-auth/dist/index.css";
import "./App.css";

const spotify = new SpotifyWebApi();

function App() {
  const dispatch = useDispatch();
  const token = useSelector((state: UserStateType) => state.user.token);

  useEffect(() => {
    const accessToken = getHash().access_token;

    if (accessToken) {
      dispatch(setToken(accessToken));

      spotify.setAccessToken(accessToken);
      spotify.getMe().then((user) => {
        dispatch(setUser(user));
      });
      spotify.getUserPlaylists().then((playlists) => {
        dispatch(setPlaylists(playlists));
      });

      document.cookie = `spotifyAuthToken=${accessToken}; max-age=${
        getHash().expires_in
      };`;
      window.localStorage.setItem("spotifyAuthToken", accessToken);
    }
  }, [dispatch, token]);

  return (
    <div className="App">
      <Router>
        {token ? (
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/search" element={<Search />} />
            <Route path="/artist/:id" element={<Artist />} />
            <Route path="/track/:id" element={<Song />} />
            <Route path="/album/:id" element={<Album />} />
            <Route path="/favorites" element={<Favorites />} />
          </Routes>
        ) : (
          <Routes>
            <Route path="/" element={<Login />} />
            <Route path="*" element={<Navigate to="/" />} />
          </Routes>
        )}
      </Router>
    </div>
  );
}

export default App;
