const clientID = "8b90776d9cc741478f7b70dfb6301f1c";
const scopes = [
  "playlist-read-private",
  "playlist-read-collaborative",
  "playlist-modify-public",
  "user-read-recently-played",
  "playlist-modify-private",
  "ugc-image-upload",
  "user-follow-modify",
  "user-follow-read",
  "user-library-modify",
  "user-read-private",
  "user-top-read",
  "user-read-playback-state",
  "user-read-email",
];
const redirectUri = "http://localhost:3000/";

const getRedirectUrl = () => {
  return (
    "https://accounts.spotify.com/authorize?" +
    `client_id=${clientID}` +
    `&scope=${scopes.join("%20")}` +
    "&response_type=token" +
    `&redirect_uri=${redirectUri}`
  );
};

export default getRedirectUrl;
