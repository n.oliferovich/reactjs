import {
  HIDE_USER_ALERT,
  PROFILE_REQUEST,
  SET_PLAYLISTS,
  SET_TOKEN,
  SET_USER,
  SHOW_USER_ALERT,
} from "../types/user.type";

const initialState = {
  token: "",
  user: {},
  playlists: [],
  profile: {},
  alert: null,
};

const userReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case SET_TOKEN:
      return { ...state, token: action.token };
    case SET_USER:
      return { ...state, user: action.user };
    case SET_PLAYLISTS:
      return { ...state, playlists: action.playlists.items };
    case PROFILE_REQUEST:
      return { ...state, profile: action.payload };
    case SHOW_USER_ALERT:
      return { ...state, alert: action.payload };
    case HIDE_USER_ALERT:
      return { ...state, alert: null };
    default:
      return state;
  }
};

export default userReducer;
