import {
  ALBUM_RESPONSE,
  ARTIST_RESPONSE,
  ARTIST_TRACKS_RESPONSE,
  FAVORITES_RESPONSE,
  HIDE_ALERT,
  HIDE_LOADER,
  NEWREALESES_RESPONSE,
  RECOMMENDATIONS_RESPONSE,
  SEARCH_RESPONSE,
  SET_PLAYING,
  SET_PLAYING_SONG,
  SHOW_ALERT,
  SHOW_LOADER,
  SONG_RESPONSE,
} from "../types/app.type";

const initialState = {
  artists: [],
  releases: [],
  search: {},
  currentSong: {},
  currentAlbum: {},
  currentArtist: {},
  currentArtistTracks: [],
  recommendations: [],
  favorites: [],
  playingSong: {},
  playing: false,
  loading: false,
  alert: null,
};

const appReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case SHOW_LOADER:
      return { ...state, loading: true };
    case HIDE_LOADER:
      return { ...state, loading: false };
    case SHOW_ALERT:
      return { ...state, alert: action.payload };
    case HIDE_ALERT:
      return { ...state, alert: null };
    case NEWREALESES_RESPONSE:
      return { ...state, releases: action.payload?.data.albums.items };
    case SEARCH_RESPONSE:
      return { ...state, search: action.payload };
    case SONG_RESPONSE:
      return { ...state, currentSong: action.payload };
    case ALBUM_RESPONSE:
      return { ...state, currentAlbum: action.payload };
    case RECOMMENDATIONS_RESPONSE:
      return { ...state, recommendations: action.payload?.tracks };
    case FAVORITES_RESPONSE:
      return { ...state, favorites: action.payload?.items };
    case SET_PLAYING_SONG:
      return { ...state, playingSong: action.song };
    case SET_PLAYING:
      return { ...state, playing: action.playing };
    case ARTIST_TRACKS_RESPONSE:
      return { ...state, currentArtistTracks: action.payload?.tracks };
    case ARTIST_RESPONSE:
      return { ...state, currentArtist: action.payload };
    default:
      return state;
  }
};

export default appReducer;
