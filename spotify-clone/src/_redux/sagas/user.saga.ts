import { call, put, takeEvery } from "redux-saga/effects";
import { hideUserAlert, showUserAlert } from "../actions/user.action";
import { UserProfileResponse } from "../../_http/responses/UserResponse";
import UserService from "../../_http/services/user.service";
import { PROFILE_REQUEST, PROFILE_RESPONSE } from "../types/user.type";

export function* userSagaWatcher() {
  yield takeEvery(PROFILE_REQUEST, sagaProfileWorker);
}

export function* sagaProfileWorker() {
  try {
    yield put(hideUserAlert());
    const payload: UserProfileResponse = yield call(getProfile);
    yield put({ type: PROFILE_RESPONSE, payload: payload.data });
  } catch (err: any) {
    yield put(showUserAlert("Server error"));
  }
}

async function getProfile() {
  return await UserService.getProfile();
}
