import { all } from "redux-saga/effects";
import { appSagaWatcher } from "./spotify.saga";
import { userSagaWatcher } from "./user.saga";

export default function* rootSaga() {
  yield all([appSagaWatcher(), userSagaWatcher()]);
}
