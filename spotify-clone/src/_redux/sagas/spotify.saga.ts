import { call, put, takeEvery } from "redux-saga/effects";
import {
  AlbumResponse,
  ArtistResponseData,
  ArtistTopTracks,
  FavoritesResponse,
  NewReleasesResponse,
  RecommendationResponse,
  SearchDataResponse,
  TrackResponse,
} from "../../_http/responses/SpotifyResponse";
import SpotifyService from "../../_http/services/app.service";
import {
  ALBUM_REQUEST,
  ALBUM_RESPONSE,
  ARTIST_REQUEST,
  ARTIST_RESPONSE,
  ARTIST_TRACKS_REQUEST,
  ARTIST_TRACKS_RESPONSE,
  FAVORITES_REQUEST,
  FAVORITES_RESPONSE,
  NEWREALESES_REQUEST,
  NEWREALESES_RESPONSE,
  RECOMMENDATIONS_REQUEST,
  RECOMMENDATIONS_RESPONSE,
  SEARCH_REQUEST,
  SEARCH_RESPONSE,
  SONG_REQUEST,
  SONG_RESPONSE,
} from "../types/app.type";
import {
  hideLoader,
  showAlert,
  showLoader,
  hideAlert,
} from "../actions/app.action";

export function* appSagaWatcher() {
  yield takeEvery(NEWREALESES_REQUEST, sagaReleaseWorker);
  yield takeEvery(SEARCH_REQUEST, sagaSearchWorker);
  yield takeEvery(SONG_REQUEST, sagaSongWorker);
  yield takeEvery(ALBUM_REQUEST, sagaAlbumWorker);
  yield takeEvery(RECOMMENDATIONS_REQUEST, sagaRecommendationWorker);
  yield takeEvery(FAVORITES_REQUEST, sagaFavoriteWorker);
  yield takeEvery(ARTIST_REQUEST, sagaArtistWorker);
  yield takeEvery(ARTIST_TRACKS_REQUEST, sagaArtistTracksWorker);
}

function* sagaReleaseWorker() {
  try {
    yield put(hideAlert());
    yield put(showLoader());
    const payload: NewReleasesResponse = yield call(getNewReleases);
    yield put({ type: NEWREALESES_RESPONSE, payload });
    yield put(hideLoader());
  } catch (err) {
    yield put(showAlert("Server error"));
    yield put(hideLoader());
  }
}

export function* sagaSearchWorker({ title }: ReturnType<any>) {
  try {
    yield put(hideAlert());
    yield put(showLoader());
    const payload: SearchDataResponse = yield call(searchSongs, title);
    yield put({ type: SEARCH_RESPONSE, payload: payload.data });
    yield put(hideLoader());
  } catch (err) {
    yield put(showAlert("Server error"));
    yield put(hideLoader());
  }
}

export function* sagaSongWorker({ id }: ReturnType<any>) {
  try {
    yield put(showLoader());
    const payload: TrackResponse = yield call(getSong, id);
    yield put({ type: SONG_RESPONSE, payload: payload.data });
    yield put(hideLoader());
  } catch (err) {
    yield put(showAlert("Server error"));
    yield put(hideLoader());
  }
}
export function* sagaAlbumWorker({ id }: ReturnType<any>) {
  try {
    yield put(showLoader());
    const payload: AlbumResponse = yield call(getAlbum, id);
    yield put({ type: ALBUM_RESPONSE, payload: payload.data });
    yield put(hideLoader());
  } catch (err) {
    yield put(showAlert("Server error"));
    yield put(hideLoader());
  }
}

export function* sagaRecommendationWorker() {
  try {
    yield put(showLoader());
    const payload: RecommendationResponse = yield call(getRecommendations);
    yield put({ type: RECOMMENDATIONS_RESPONSE, payload: payload.data });
    yield put(hideLoader());
  } catch (err) {
    yield put(showAlert("Server error"));
    yield put(hideLoader());
  }
}

export function* sagaFavoriteWorker() {
  try {
    yield put(showLoader());
    const payload: FavoritesResponse = yield call(getFavorites);
    yield put({ type: FAVORITES_RESPONSE, payload: payload.data });
    yield put(hideLoader());
  } catch (err) {
    yield put(showAlert("Server error"));
    yield put(hideLoader());
  }
}

export function* sagaArtistTracksWorker({ id, country }: ReturnType<any>) {
  try {
    yield put(showLoader());
    const payload: ArtistTopTracks = yield call(
      getArtistTopTracks,
      id,
      country
    );
    yield put({ type: ARTIST_TRACKS_RESPONSE, payload: payload.data });
    yield put(hideLoader());
  } catch (err) {
    yield put(showAlert("Server error"));
    yield put(hideLoader());
  }
}

export function* sagaArtistWorker({ id }: ReturnType<any>) {
  try {
    yield put(showLoader());
    const payload: ArtistResponseData = yield call(getArtist, id);
    yield put({ type: ARTIST_RESPONSE, payload: payload.data });
    yield put(hideLoader());
  } catch (err) {
    yield put(showAlert("Server error"));
    yield put(hideLoader());
  }
}

async function getNewReleases() {
  return await SpotifyService.getNewReleases();
}

async function searchSongs(title: string) {
  return await SpotifyService.searchSongs(title);
}

async function getSong(id: string) {
  return await SpotifyService.getTrack(id);
}

async function getAlbum(id: string) {
  return await SpotifyService.getAlbum(id);
}

async function getRecommendations() {
  return await SpotifyService.getRecomendations();
}

async function getFavorites() {
  return await SpotifyService.getFavorites();
}

async function getArtistTopTracks(id: string, country: string) {
  return await SpotifyService.getArtistSongs(id, country);
}

async function getArtist(id: string) {
  return await SpotifyService.getArtist(id);
}
