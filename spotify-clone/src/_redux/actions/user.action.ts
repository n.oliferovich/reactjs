import {
  HIDE_USER_ALERT,
  PROFILE_REQUEST,
  SET_PLAYLISTS,
  SET_TOKEN,
  SET_USER,
  SHOW_USER_ALERT,
} from "../types/user.type";

export function getProfile() {
  return {
    type: PROFILE_REQUEST,
  };
}

export function showUserAlert(text: string) {
  return {
    type: SHOW_USER_ALERT,
    payload: text,
  };
}

export function hideUserAlert() {
  return {
    type: HIDE_USER_ALERT,
  };
}

export function setToken(accessToken: string) {
  return {
    type: SET_TOKEN,
    token: accessToken,
  };
}

export function setPlaylists(playlists: any) {
  return {
    type: SET_PLAYLISTS,
    playlists,
  };
}

export function setUser(user: any) {
  return {
    type: SET_USER,
    user,
  };
}
