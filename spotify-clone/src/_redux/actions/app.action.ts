import { Track } from "../../_http/responses/SpotifyResponse";
import {
  ALBUM_REQUEST,
  ARTIST_REQUEST,
  ARTIST_TRACKS_REQUEST,
  FAVORITES_REQUEST,
  HIDE_ALERT,
  HIDE_LOADER,
  NEWREALESES_REQUEST,
  RECOMMENDATIONS_REQUEST,
  SEARCH_REQUEST,
  SET_PLAYING,
  SET_PLAYING_SONG,
  SHOW_ALERT,
  SHOW_LOADER,
  SONG_REQUEST,
} from "../types/app.type";

export function showAlert(text: string) {
  return {
    type: SHOW_ALERT,
    payload: text,
  };
}

export function hideAlert() {
  return {
    type: HIDE_ALERT,
  };
}

export function showLoader() {
  return {
    type: SHOW_LOADER,
  };
}

export function hideLoader() {
  return {
    type: HIDE_LOADER,
  };
}

export function getArtist(id: string) {
  return {
    type: ARTIST_REQUEST,
    id,
  };
}

export function getArtistSongs(id: string, country: string) {
  return {
    type: ARTIST_TRACKS_REQUEST,
    id,
    country,
  };
}

export function getNewReleases() {
  return {
    type: NEWREALESES_REQUEST,
  };
}

export function searchSongs(title: string) {
  return {
    type: SEARCH_REQUEST,
    title,
  };
}

export function getSong(id: string) {
  return {
    type: SONG_REQUEST,
    id,
  };
}

export function getAlbum(id: string) {
  return {
    type: ALBUM_REQUEST,
    id,
  };
}

export function getFavorites() {
  return {
    type: FAVORITES_REQUEST,
  };
}

export function getRecommendations() {
  return {
    type: RECOMMENDATIONS_REQUEST,
  };
}

export function setPlayingSong(song: Track) {
  return {
    type: SET_PLAYING_SONG,
    song,
  };
}

export function setPlaying(playing: boolean) {
  return {
    type: SET_PLAYING,
    playing,
  };
}
