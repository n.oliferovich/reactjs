export const ARTIST_REQUEST = "APP/ARTIST_REQUEST";
export const ARTIST_RESPONSE = "APP/ARTIST_RESPONSE";
export const NEWREALESES_REQUEST = "APP/NEWREALESES_REQUEST";
export const NEWREALESES_RESPONSE = "APP/NEWREALESES_RESPONSE";
export const RECOMMENDATIONS_REQUEST = "APP/RECOMMENDATIONS_REQUEST";
export const RECOMMENDATIONS_RESPONSE = "APP/RECOMMENDATIONS_RESPONSE";
export const SHOW_ALERT = "APP/SHOW_ALERT";
export const HIDE_ALERT = "APP/HIDE_ALERT";
export const SHOW_LOADER = "APP/SHOW_LOADER";
export const HIDE_LOADER = "APP/HIDE_LOADER";
export const SEARCH_REQUEST = "APP/SEARCH_REQUEST";
export const SEARCH_RESPONSE = "APP/SEARCH_RESPONSE";
export const SONG_REQUEST = "APP/SONG_REQUEST";
export const SONG_RESPONSE = "APP/SONG_RESPONSE";
export const ALBUM_REQUEST = "APP/ALBUM_REQUEST";
export const ALBUM_RESPONSE = "APP/ALBUM_RESPONSE";
export const FAVORITES_REQUEST = "APP/FAVORITES_REQUEST";
export const FAVORITES_RESPONSE = "APP/FAVORITES_RESPONSE";
export const SET_PLAYING_SONG = "APP/SET_PLAYING_SONG";
export const SET_PLAYING = "APP/SET_PLAYING";
export const ARTIST_TRACKS_REQUEST = "APP/ARTIST_TRACKS_REQUEST";
export const ARTIST_TRACKS_RESPONSE = "APP/ARTIST_TRACKS_RESPONSE";
