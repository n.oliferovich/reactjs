import { useEffect } from "react";
import { useParams } from "react-router-dom";
import ReactAudioPlayer from "react-audio-player";
import { Sidebar } from "../../components/Sidebar/Sidebar";
import { User } from "../../components/User/User";
import { Footer } from "../../components/Footer/Footer";
import { useDispatch, useSelector } from "react-redux";
import { getSong } from "../../_redux/actions/app.action";
import { StateType } from "../../_http/responses/SpotifyResponse";

import { useMessage } from "../../hooks/message.hook";
import "./Song.css";

const Song = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const song = useSelector((state: StateType) => state.app.currentSong);
  const alert = useSelector((state: StateType) => state.app.alert);
  const message = useMessage();

  useEffect(() => {
    message(alert);
  }, [alert, message]);

  useEffect(() => {
    if (id) dispatch(getSong(id));
  }, [dispatch, id]);

  return (
    <div>
      <div className="home__body">
        <Sidebar />
        <div className="body">
          <User />
          <div className="body__info">
            <div className="body__infoText">
              <strong>SONG</strong>
              <h2>{song?.name}</h2>
              <p>{song.popularity} likes</p>
            </div>
          </div>

          <div className="body__songs">
            <ReactAudioPlayer src={song?.preview_url} controls />
          </div>
          <div className="body__related_songs">
            <h2>Related songs</h2>
            <div className="related__songs"></div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export { Song };
