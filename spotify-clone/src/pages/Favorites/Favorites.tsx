import { useDispatch, useSelector } from "react-redux";
import { Loader } from "../../components/Loader/Loader";
import { Sidebar } from "../../components/Sidebar/Sidebar";
import { SongItem } from "../../components/SongItem/SongItem";
import { User } from "../../components/User/User";
import { StateType, Track } from "../../_http/responses/SpotifyResponse";
import likedImg from "../../assets/img/liked.png";
import { useEffect, useMemo } from "react";
import { getFavorites } from "../../_redux/actions/app.action";
import { Footer } from "../../components/Footer/Footer";
import { useMessage } from "../../hooks/message.hook";

const Favorites = () => {
  const dispatch = useDispatch();
  const loading = useSelector((state: StateType) => state.app.loading);
  const favorites = useSelector((state: StateType) => state.app.favorites);
  const alert = useSelector((state: StateType) => state.app.alert);
  const message = useMessage();

  const favoritesSongs = useMemo(() => {
    return favorites?.map((track: Track) => (
      <SongItem song={track} key={track.id} />
    ));
  }, [favorites]);

  useEffect(() => {
    message(alert);
  }, [alert, message]);

  useEffect(() => {
    dispatch(getFavorites());
  }, [dispatch]);

  return (
    <div className="album">
      <div className="home__body">
        <Sidebar />
        <div className="body">
          <User />
          <div className="body__info">
            <div className="body__info_img">
              <img src={likedImg} alt="Favorites" />
            </div>
            <div className="body__infoText">
              <strong>ALBUM</strong>
              <h2>Favorites</h2>
            </div>
          </div>

          <div className="body__songs">
            <div className="row">
              {favorites.length ? (
                loading ? (
                  <Loader />
                ) : (
                  favoritesSongs
                )
              ) : (
                <div className="py-2">No Liked Songs</div>
              )}
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export { Favorites };
