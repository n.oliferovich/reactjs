import { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { Footer } from "../../components/Footer/Footer";
import { Loader } from "../../components/Loader/Loader";
import { Sidebar } from "../../components/Sidebar/Sidebar";
import { SongItem } from "../../components/SongItem/SongItem";
import { User } from "../../components/User/User";
import { useMessage } from "../../hooks/message.hook";
import { getAlbum } from "../../_redux/actions/app.action";
import { StateType, Track } from "../../_http/responses/SpotifyResponse";
import "./Album.css";

const Album = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const album = useSelector((state: StateType) => state.app.currentAlbum);
  const loading = useSelector((state: StateType) => state.app.loading);
  const alert = useSelector((state: StateType) => state.app.alert);
  const message = useMessage();

  const albumSongs = useMemo(() => {
    return album?.tracks?.items?.map((track: Track) => (
      <SongItem song={track} key={track.id} />
    ));
  }, [album]);

  useEffect(() => {
    message(alert);
  }, [alert, message]);

  useEffect(() => {
    if (id) dispatch(getAlbum(id));
  }, [dispatch, id]);

  return (
    <div className="album">
      <div className="home__body">
        <Sidebar />
        <div className="body">
          <User />
          <div className="body__info">
            <div className="body__info_img">
              <img
                src={album.images && album.images[0]?.url}
                alt={album.name}
              />
            </div>
            <div className="body__infoText">
              <strong>ALBUM</strong>
              <h2>{album.name}</h2>
              <p>
                {album.label} · {album.popularity} likes
              </p>
            </div>
          </div>

          <div className="body__songs">
            <div className="row">{loading ? <Loader /> : albumSongs}</div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export { Album };
