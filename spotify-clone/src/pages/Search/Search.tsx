import { useEffect, useMemo } from "react";
import { useSelector } from "react-redux";
import { Footer } from "../../components/Footer/Footer";
import { Loader } from "../../components/Loader/Loader";
import { SearchForm } from "../../components/SearchForm/SearchForm";
import { Sidebar } from "../../components/Sidebar/Sidebar";
import { SongItem } from "../../components/SongItem/SongItem";
import { User } from "../../components/User/User";
import { useMessage } from "../../hooks/message.hook";
import { StateType, Track } from "../../_http/responses/SpotifyResponse";
import "./Search.css";

const Search = () => {
  const alert = useSelector((state: StateType) => state.app.alert);
  const loading = useSelector((state: StateType) => state.app.loading);
  const results = useSelector((state: StateType) => state.app.search);
  const message = useMessage();

  const resultsSongs = useMemo(() => {
    return results?.tracks?.items?.map((track: Track) => (
      <SongItem song={track} key={track.id} />
    ));
  }, [results]);

  useEffect(() => {
    message(alert);
  }, [alert, message]);

  return (
    <div>
      <div className="home__body">
        <Sidebar />
        <div className="body">
          <User />
          <div className="body__info">
            <div className="body__infoText">
              <strong>SEARCH</strong>
              <SearchForm />
            </div>
          </div>

          <div className="body__songs">
            {loading ? (
              <Loader />
            ) : (
              <div>
                <h4 className="songRow">Songs</h4>
                <div>{resultsSongs}</div>
              </div>
            )}
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export { Search };
