import { useEffect, useMemo } from "react";
import { useParams } from "react-router-dom";
import { Sidebar } from "../../components/Sidebar/Sidebar";
import { User } from "../../components/User/User";
import { useDispatch, useSelector } from "react-redux";
import { getArtist, getArtistSongs } from "../../_redux/actions/app.action";
import { StateType, Track } from "../../_http/responses/SpotifyResponse";
import { Footer } from "../../components/Footer/Footer";
import { useMessage } from "../../hooks/message.hook";
import { UserStateType } from "../../_http/responses/UserResponse";
import { SongItem } from "../../components/SongItem/SongItem";
import { Loader } from "../../components/Loader/Loader";
import "../Song/Song.css";

const Artist = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const artist = useSelector((state: StateType) => state.app.currentArtist);
  const artistSongs = useSelector(
    (state: StateType) => state.app.currentArtistTracks
  );
  const alert = useSelector((state: StateType) => state.app.alert);
  const country = useSelector(
    (state: UserStateType) => state.user.user.country
  );
  const loading = useSelector((state: StateType) => state.app.loading);
  const message = useMessage();

  const songs = useMemo(() => {
    return artistSongs?.map((song: Track) => (
      <SongItem song={song} key={song.id} />
    ));
  }, [artistSongs]);

  useEffect(() => {
    message(alert);
  }, [alert, message]);

  useEffect(() => {
    if (id) dispatch(getArtist(id));
  }, [dispatch, id]);

  useEffect(() => {
    if (id && country) dispatch(getArtistSongs(id, country));
  }, [country, dispatch, id]);

  return (
    <div>
      <div className="home__body">
        <Sidebar />
        <div className="body">
          <User />
          <div className="body__info">
            <div className="body__infoText">
              <strong>Artist</strong>
              <h2>{artist?.name}</h2>
              <p>{artist?.popularity} likes</p>
            </div>
          </div>

          <div className="body__related_songs">
            <h2>Songs</h2>
            <div className="related__songs">{loading ? <Loader /> : songs}</div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export { Artist };
