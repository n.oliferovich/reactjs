import { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getNewReleases,
  getRecommendations,
} from "../../_redux/actions/app.action";
import {
  ReleaseResponse,
  StateType,
  Track,
} from "../../_http/responses/SpotifyResponse";
import { Loader } from "../../components/Loader/Loader";
import "./Home.css";
import { useMessage } from "../../hooks/message.hook";
import { Sidebar } from "../../components/Sidebar/Sidebar";
import { SongItem } from "../../components/SongItem/SongItem";
import { User } from "../../components/User/User";
import { AlbumItem } from "../../components/AlbumItem/AlbumItem";
import { Footer } from "../../components/Footer/Footer";

const Home = () => {
  const dispatch = useDispatch();
  const alert = useSelector((state: StateType) => state.app.alert);
  const releases = useSelector((state: StateType) => state.app.releases);
  const recommendations = useSelector(
    (state: StateType) => state.app.recommendations
  );
  const loading = useSelector((state: StateType) => state.app.loading);
  const message = useMessage();

  const releasesSongs = useMemo(() => {
    return releases?.map((release: ReleaseResponse) => (
      <AlbumItem album={release} key={release.id} />
    ));
  }, [releases]);

  const recommendationsSongs = useMemo(() => {
    return recommendations?.map((track: Track) => (
      <SongItem song={track} key={track.id} />
    ));
  }, [recommendations]);

  useEffect(() => {
    message(alert);
  }, [alert, message]);

  useEffect(() => {
    dispatch(getNewReleases());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getRecommendations());
  }, [dispatch]);

  return (
    <div>
      <div className="home__body">
        <Sidebar />
        <div className="body">
          <User />
          <div className="body__info">
            <div className="body__infoText">
              <strong>Home</strong>
              <h2>New Releases</h2>
            </div>
          </div>

          <div className="body__songs">
            <div className="row">{loading ? <Loader /> : releasesSongs}</div>
          </div>
          <div className="body__info">
            <div className="body__infoText">
              <h2>Recommendations</h2>
            </div>
          </div>

          <div className="body__songs">
            <div className="row">
              {loading ? <Loader /> : recommendationsSongs}
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export { Home };
