import getRedirectUrl from "../../generateUrl";
import spotifyLogo from "../../assets/img/spotify2019-830x350.jpg";
import "./Login.css";

const Login = () => {
  const redirectUri = getRedirectUrl();

  return (
    <div className="login">
      <img src={spotifyLogo} alt="Spotify" />

      <a href={redirectUri}>LOGIN WITH SPOTIFY</a>
    </div>
  );
};

export { Login };
