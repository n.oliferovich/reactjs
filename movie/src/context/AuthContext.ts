import { createContext } from "react";

export const AuthContext = createContext({
  userLogin: "",
  isAuth: false,
  login: (login: string) => {},
  logout: () => {},
});
