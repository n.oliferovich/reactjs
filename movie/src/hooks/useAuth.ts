import { useState, useCallback, useEffect } from "react";

const storageName = "currentUser";

export const useAuth = () => {
  const [userLogin, setUserLogin] = useState("");

  const login = useCallback((login) => {
    setUserLogin(login);

    localStorage.setItem(
      storageName,
      JSON.stringify({
        userId: login,
      })
    );
  }, []);

  const logout = useCallback(() => {
    setUserLogin("");
    localStorage.removeItem(storageName);
  }, []);

  useEffect(() => {
    const data = JSON.parse(localStorage.getItem(storageName) || "{}");

    if (data?.userId) {
      login(data.userId);
    }
  }, [login]);

  return { login, logout, userLogin };
};
