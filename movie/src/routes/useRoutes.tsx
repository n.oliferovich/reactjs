import { Routes, Route, Navigate } from "react-router-dom";
import { MovieList } from "../components/MovieList";
import { MoviePage } from "../components/MoviePage";
import { Login } from "../components/Login";
import { LikedMovies } from "../components/LikedMovies";
import { Lists } from "../components/Lists";
import { NewList } from "../components/NewList";
import { List } from "../components/List";

export const useRoutes = (isAuth: boolean) => {
  if (isAuth) {
    return (
      <Routes>
        <Route path="/" element={<MovieList />} />
        <Route path="/:id" element={<MoviePage />} />
        <Route path="/login" element={<Navigate to="/" />} />
        <Route path="/liked" element={<LikedMovies />} />
        <Route path="/lists" element={<Lists />} />
        <Route path="/lists/newlist" element={<NewList />} />
        <Route path="/lists/:id" element={<List />} />
      </Routes>
    );
  } else {
    return (
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/" element={<Navigate to="/login" />} />
      </Routes>
    );
  }
};
