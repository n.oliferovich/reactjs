import { initializeApp } from "firebase/app";
import {
  getFirestore,
  collection,
  getDocs,
  setDoc,
  deleteDoc,
  doc,
  query,
  where,
} from "firebase/firestore";
import { getAuth } from "firebase/auth";
import { LikedType } from "./components/LikedMovies";
import { ListType } from "./components/Lists";

const firebaseConfig = {
  apiKey: "AIzaSyDa3D4Ia7a2N9n8f0gobT6po6m4z7Nw2YU",
  authDomain: "movie-88959.firebaseapp.com",
  projectId: "movie-88959",
  storageBucket: "movie-88959.appspot.com",
  messagingSenderId: "547783122043",
  appId: "1:547783122043:web:219a7848b38607e5b796de",
  measurementId: "G-M2WG4MHQDP",
};

const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const auth = getAuth(app);

export async function setLikes(userId: string, id: string, name: string) {
  const likeMovie = await setDoc(doc(db, "likes", id), {
    id,
    name,
    userId,
  });

  return likeMovie;
}

export async function getLikes(userId: string) {
  const movieSnapshot = await getDocs(
    query(collection(db, "likes"), where("userId", "==", userId))
  );
  const likedList = movieSnapshot.docs.map((doc) => doc.data() as LikedType);

  return likedList;
}

export async function deleteLike(id: string) {
  await deleteDoc(doc(db, "likes", id));

  return;
}

export async function addList(name: string, userId: string) {
  const list = await setDoc(doc(db, "lists", `${userId}_${name}`), {
    id: `${userId}_${name}`,
    name,
    userId,
    movies: [],
  });

  return list;
}

export async function getLists(userId: string) {
  const listSnapshot = await getDocs(
    query(collection(db, "lists"), where("userId", "==", userId))
  );
  const lists = listSnapshot.docs.map((doc) => doc.data() as ListType);

  return lists;
}
