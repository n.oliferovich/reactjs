import { BrowserRouter as Router } from "react-router-dom";
import { useRoutes } from "./routes/useRoutes";
import { useAuth } from "./hooks/useAuth";
import { AuthContext } from "./context/AuthContext";
import { Footer } from "./components/Footer";
import { Header } from "./components/Header";
import "material-icons/iconfont/material-icons.css";
import "materialize-css";
import "./App.css";

function App() {
  const { login, logout, userLogin } = useAuth();
  const isAuth = !!userLogin;
  const routes = useRoutes(isAuth);

  return (
    <div className="App">
      <AuthContext.Provider value={{ userLogin, isAuth, login, logout }}>
        <Router>
          <header>
            <Header />
          </header>
          <main>
            <div className="container">{routes}</div>
          </main>
          <footer className="page-footer blue darken-1">
            <Footer />
          </footer>
        </Router>
      </AuthContext.Provider>
    </div>
  );
}

export default App;
