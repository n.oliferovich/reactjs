import { useEffect, useState, useContext } from "react";
import { NavLink } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";
import { getLists } from "../firebase-config";
import { User } from "./User";

export type ListType = {
  id: string;
  name: string;
  userId: string;
  movies: string[];
};

export const Lists = () => {
  const context = useContext(AuthContext);
  const [lists, setLists] = useState<Array<ListType>>();

  useEffect(() => {
    async function getUserLists() {
      const movies = await getLists(context.userLogin);
      setLists(movies);
    }
    getUserLists();
  }, [context.userLogin]);

  return (
    <div className="row">
      <div className="col s12 m12 l3">
        <User />
      </div>
      <div className="col s12 m12 l9">
        <div className="list">
          <h4>My movie lists</h4>
          <br />
          <div className="row">
            <div className="col s12 m4">
              <div className="add-new-list">
                <NavLink to="/lists/newlist" className="newlistblock">
                  <i className="material-icons">add</i>
                </NavLink>
              </div>
            </div>
            {lists?.map((list: ListType) => {
              return (
                <div className="col s12 m4" key={list.id}>
                  <div className="list-item">
                    <NavLink to={`lists/${list.id}`}>{list.name}</NavLink>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};
