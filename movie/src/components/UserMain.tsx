import { useContext } from "react";
import { NavLink } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";

export const UserMain = () => {
  const context = useContext(AuthContext);

  return (
    <div className="user">
      <div className="user-img">
        <img src="/user.png" alt={context.userLogin} />
      </div>
      <div className="user-info">
        <div className="user-name">
          <p>ID: {context.userLogin}</p>
          <br />
          <NavLink to="/liked" className="liked-link">
            {" "}
            <i className="material-icons">favorite</i> Liked movies
          </NavLink>

          <NavLink to="/lists" className="mylist">
            <i className="material-icons">add</i> Мои списки
          </NavLink>
        </div>
      </div>
    </div>
  );
};
