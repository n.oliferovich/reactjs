import { useContext } from "react";
import { NavLink } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";

export const UserList = () => {
  const context = useContext(AuthContext);

  return (
    <div className="user">
      <div className="back">
        <NavLink to="/lists">
          {" "}
          <i className="material-icons">chevron_left</i> Назад
        </NavLink>
      </div>
      <br />
      <div className="user-img">
        <img src="/user.png" alt={context.userLogin} />
      </div>
      <div className="user-info">
        <div className="user-name">
          <p>ID: {context.userLogin}</p>
          <br />
          <NavLink to="/liked" className="liked-link">
            {" "}
            <i className="material-icons">favorite</i> Liked movies
          </NavLink>

          <NavLink to="/lists" className="mylist">
            <i className="material-icons">add</i> Мои списки
          </NavLink>
        </div>
      </div>
    </div>
  );
};
