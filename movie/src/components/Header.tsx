import { useContext, MouseEvent } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";
import { signOut } from "firebase/auth";
import { auth } from "../firebase-config";

export const Header = () => {
  const history = useNavigate();
  const context = useContext(AuthContext);

  const logoutHandler = async (event: MouseEvent) => {
    event.preventDefault();
    await signOut(auth);
    context.logout();
    history("/login");
  };

  if (context.isAuth) {
    return (
      <nav>
        <div
          className="nav-wrapper blue darken-1"
          style={{ padding: "0 2rem" }}
        >
          <span className="brand-logo">VironIt</span>
          <ul id="nav-mobile" className="right hide-on-med-and-down">
            <li>
              <p onClick={logoutHandler}>Выйти</p>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
  return (
    <nav>
      <div className="nav-wrapper blue darken-1" style={{ padding: "0 2rem" }}>
        <span className="brand-logo">VironIt</span>
        <ul id="nav-mobile" className="right hide-on-med-and-down">
          <li>
            <NavLink to="/login">Войти</NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};
