import axios from "axios";
import { useEffect, useContext, useState } from "react";
import { AuthContext } from "../context/AuthContext";
import { getLikes } from "../firebase-config";
import { LikedItem } from "./LikedItem";
import { headers, MovieType } from "./MovieList";
import { User } from "./User";

export type LikedType = {
  id: string;
  name: string;
  userId: string;
};

export const LikedMovies = () => {
  const context = useContext(AuthContext);

  const [likedMovies, setLikedMovies] = useState<Array<LikedType>>([]);
  const [moviesInfo, setMoviesInfo] = useState<Array<MovieType>>([]);

  useEffect(() => {
    async function getMovies() {
      const movies = await getLikes(context.userLogin);
      setLikedMovies(movies);
    }
    getMovies();
  }, [context.userLogin]);

  useEffect(() => {
    setMoviesInfo([]);
    async function getMovie(id: string) {
      const movie = await axios.get(
        `https://data-imdb1.p.rapidapi.com/titles/${id}`,
        {
          headers: headers,
          params: {
            info: "mini_info",
          },
        }
      );

      return movie.data.results;
    }

    if (likedMovies.length) {
      likedMovies.forEach(async (movie) => {
        const m = await getMovie(movie.id);
        setMoviesInfo((prevState) => [...prevState, m]);
        return movie;
      });
    }
  }, [likedMovies]);

  return (
    <div className="row">
      <div className="col s12 m12 l3">
        <User />
      </div>
      <div className="col s12 m12 l9">
        <div className="like-page">
          <h2>Liked movies</h2>
          <div className="row">
            {moviesInfo.length ? (
              moviesInfo.map((movie) => {
                return <LikedItem movie={movie} key={movie.id} />;
              })
            ) : (
              <p>Нет результатов</p>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
