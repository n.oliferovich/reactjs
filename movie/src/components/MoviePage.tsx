import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";
import { setLikes } from "../firebase-config";
import { headers, MovieType } from "./MovieList";
import { User } from "./User";

export const MoviePage = () => {
  const context = useContext(AuthContext);
  let { id } = useParams();
  const [isLoaded, setIsLoaded] = useState(true);
  const [movieData, setMovieData] = useState<MovieType>();
  const [rating, setRating] = useState("");
  const [seasons, setSeasons] = useState(0);
  const [isLikes, setIsLiked] = useState(false);

  const addToLikeList = async () => {
    if (movieData) {
      await setLikes(
        context.userLogin,
        movieData.id,
        movieData.titleText.text
      ).then(() => {
        setIsLiked(true);
      });
    }
  };

  useEffect(() => {
    async function getData() {
      await axios
        .get(`https://data-imdb1.p.rapidapi.com/titles/${id}`, {
          headers: headers,
          params: { info: "mini_info" },
        })
        .then((res) => {
          setIsLoaded(true);
          setMovieData(res.data.results);
        })
        .catch((err) => {
          setIsLoaded(true);
          console.log(err);
        });
    }

    getData();
  }, [id]);

  useEffect(() => {
    async function getRating() {
      await axios
        .get(`https://data-imdb1.p.rapidapi.com/titles/${id}/ratings`, {
          headers: {
            "x-rapidapi-host": "data-imdb1.p.rapidapi.com",
            "x-rapidapi-key":
              "698da318e5msh6a3402e18b4b41bp140b8djsn911e5cdfba01",
          },
        })
        .then((res) => {
          setIsLoaded(true);
          setRating(res.data.results?.averageRating);
        })
        .catch((err) => {
          setIsLoaded(true);
          console.log(err);
        });
    }

    getRating();
  }, [id]);

  useEffect(() => {
    async function getSeasons() {
      await axios
        .get(`https://data-imdb1.p.rapidapi.com/titles/seasons/${id}`, {
          headers: {
            "x-rapidapi-host": "data-imdb1.p.rapidapi.com",
            "x-rapidapi-key":
              "698da318e5msh6a3402e18b4b41bp140b8djsn911e5cdfba01",
          },
        })
        .then(() => {
          setIsLoaded(true);
        })
        .catch((err) => {
          console.log(err);
          setSeasons(0);
        });
    }

    if (movieData?.titleType.isSeries) getSeasons();
  }, [id, movieData]);

  return (
    <div className="row">
      <div className="col s12 m12 l3">
        <User />
      </div>
      <div className="col s12 m12 l9">
        <div className="movie">
          {(!isLoaded && <div>Загрузка...</div>) || (
            <div className="movie-content">
              <div className="row">
                {movieData && (
                  <div className="movie-item col s12 m12">
                    <h2 className="header">{movieData.titleText.text}</h2>
                    <br />
                    <div className="card horizontal">
                      <div className="card-image">
                        <a href={movieData.id}>
                          <img
                            src={
                              movieData.primaryImage
                                ? movieData.primaryImage.url
                                : "/no-image.png"
                            }
                            alt={movieData.titleText.text}
                          />
                        </a>
                      </div>
                      <div className="card-stacked">
                        <div className="card-content">
                          {rating && (
                            <p>
                              Rating: {rating}{" "}
                              <span className="icon-star">
                                <i className="material-icons">star</i>
                              </span>
                            </p>
                          )}
                          {movieData.releaseDate && (
                            <p>
                              Release Date: {movieData.releaseDate.day}.
                              {movieData.releaseDate.month}.
                              {movieData.releaseDate.year}
                            </p>
                          )}
                          {seasons > 0 && <p>Seasons: {seasons}</p>}
                          {movieData.titleType.id && (
                            <p>Type: {movieData.titleType.id}</p>
                          )}
                        </div>
                        <div className="card-action">
                          <div
                            className="like-movie btn-floating halfway-fab waves-effect waves-light red"
                            onClick={addToLikeList}
                          >
                            <i className="material-icons">
                              {isLikes ? "remove" : "add"}
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
