import React, { useEffect, useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";
import { addList } from "../firebase-config";
import { UserList } from "./UserList";

export const NewList = () => {
  const context = useContext(AuthContext);
  const [title, setTitle] = useState("");
  const history = useNavigate();

  const saveList = async (e: React.FormEvent) => {
    e.preventDefault();
    if (title) {
      await addList(title, context.userLogin).then(() => {
        history("/lists");
      });
    }
  };

  return (
    <div className="row">
      <div className="col s12 m12 l3">
        <UserList />
      </div>
      <div className="col s12 m12 l9">
        <div className="list">
          <h4>Create new list</h4>
          <br />
          <form onSubmit={saveList}>
            <div className="input-field col s12 m9 l10">
              <input
                placeholder="Input list title"
                id="title"
                type="text"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
              <label htmlFor="title" className="active">
                Title
              </label>
            </div>
            <div className="input-field col s12 m3 l2">
              <button className="btn btn-search" onClick={saveList}>
                Save
              </button>
            </div>
          </form>
          <div className="list-movies"></div>
          <div className="add-to-list"></div>
        </div>
      </div>
    </div>
  );
};
