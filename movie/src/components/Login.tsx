import { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
} from "firebase/auth";
import { AuthContext } from "../context/AuthContext";
import { auth } from "../firebase-config";

export const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const context = useContext(AuthContext);
  const history = useNavigate();

  const clearForm = () => {
    setEmail("");
    setPassword("");
  };

  const register = async () => {
    try {
      if (email && password) {
        const user = await createUserWithEmailAndPassword(
          auth,
          email,
          password
        );
        context.login(user.user.uid);
        clearForm();
        history("/");
      }
    } catch (err) {
      console.log(err);
      clearForm();
    }
  };

  const login = async (e: React.FormEvent<EventTarget>) => {
    e.preventDefault();
    try {
      if (email && password) {
        const user = await signInWithEmailAndPassword(auth, email, password);
        context.login(user.user.uid);
        clearForm();
        history("/");
      }
    } catch (err) {
      console.log(err);
      clearForm();
    }
  };

  return (
    <div className="login">
      <div className="row">
        <div className="col s6 offset-s3">
          <form className="card blue darken-1" onSubmit={login}>
            <div className="card-content white-text">
              <span className="card-title">Авторизация</span>
              <div>
                <div className="input-field">
                  <input
                    id="email"
                    type="text"
                    name="email"
                    className="yellow-input"
                    onChange={(e) => setEmail(e.target.value)}
                    value={email}
                  />
                  <label htmlFor="email">Email</label>
                </div>
                <div className="input-field">
                  <input
                    id="password"
                    type="password"
                    name="password"
                    className="yellow-input"
                    onChange={(e) => setPassword(e.target.value)}
                    value={password}
                  />
                  <label htmlFor="password">Пароль</label>
                </div>
              </div>
            </div>
            <div className="card-action">
              <button
                className="btn yellow darken-4"
                style={{ marginRight: 10 }}
                onClick={login}
                type="submit"
              >
                Войти
              </button>
              <button
                className="btn grey lighten-1 black-text"
                onClick={register}
              >
                Регистрация
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
