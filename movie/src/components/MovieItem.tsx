import { useContext, useState } from "react";
import { setLikes } from "../firebase-config";
import { MovieType } from "./MovieList";
import { AuthContext } from "../context/AuthContext";

export const MovieItem = ({ movie }: { movie: MovieType }) => {
  const context = useContext(AuthContext);
  const [isLikes, setIsLiked] = useState(false);

  const addLiked = async () => {
    await setLikes(context.userLogin, movie.id, movie.titleText.text).then(
      () => {
        setIsLiked(true);
      }
    );
  };

  return (
    <div className="movie-item col s12 m8">
      <h4 className="header">{movie.titleText.text}</h4>
      <div className="card horizontal">
        <div className="card-image">
          <a href={movie.id}>
            <img
              src={
                movie.primaryImage ? movie.primaryImage.url : "/no-image.png"
              }
              alt={movie.titleText.text}
            />
          </a>
          <div
            className="add-movie-rand btn-floating halfway-fab waves-effect waves-light red"
            onClick={addLiked}
          >
            <i className="material-icons">{isLikes ? "remove" : "add"}</i>
          </div>
        </div>
        <div className="card-stacked">
          <div className="card-content">
            {movie.releaseDate && (
              <p>
                Release Date: {movie.releaseDate.day}.{movie.releaseDate.month}.
                {movie.releaseDate.year}
              </p>
            )}
          </div>
          <div className="card-action">
            <a href={movie.id}>Learn more...</a>
          </div>
        </div>
      </div>
    </div>
  );
};
