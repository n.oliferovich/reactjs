import axios from "axios";
import { FormEvent, useState } from "react";
import { MovieItem } from "./MovieItem";
import { SearchItem } from "./SearchItem";
import { UserMain } from "./UserMain";

export type MovieType = {
  id: string;
  primaryImage: {
    url: string;
  };
  titleText: {
    text: string;
  };
  titleType: {
    id: string;
    isSeries: boolean;
    isEpisode: boolean;
  };
  releaseDate: {
    day: number;
    month: number;
    year: number;
  };
};

export const headers = {
  "x-rapidapi-host": "data-imdb1.p.rapidapi.com",
  "x-rapidapi-key": "698da318e5msh6a3402e18b4b41bp140b8djsn911e5cdfba01",
};

export const MovieList = () => {
  const [movieList, setMovieList] = useState<Array<MovieType>>([]);
  const [search, setSearch] = useState("");
  const [searchList, setSearchList] = useState<Array<MovieType>>([]);

  const randomButtonHandler = async () => {
    await axios
      .get("https://data-imdb1.p.rapidapi.com/titles", {
        headers: headers,
        params: {
          info: "mini_info",
          limit: "1",
          page: Math.floor(Math.random() * 100000),
          titleType: "movie",
        },
      })
      .then((res) => {
        setMovieList(res.data.results);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const searchHandler = async (e: FormEvent) => {
    e.preventDefault();

    if (search) {
      await axios
        .get(
          `https://data-imdb1.p.rapidapi.com/titles/search/keyword/${search}`,
          {
            headers: headers,
            params: {
              info: "mini_info",
              limit: "9",
              page: "1",
            },
          }
        )
        .then((res) => {
          setSearchList(res.data.results);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  return (
    <div className="row">
      <div className="col s12 m12 l3">
        <UserMain />
      </div>
      <div className="col s12 m12 l9">
        <div className="movie-list">
          {
            <div className="movies">
              <h4>Get random movie</h4>
              <button
                className="waves-effect waves-light btn"
                onClick={randomButtonHandler}
              >
                Random
              </button>
              <br />
              <div className="row">
                {movieList?.map((movie: any) => (
                  <MovieItem movie={movie} key={movie.id} />
                ))}
              </div>
            </div>
          }
          <h4>Get movie by searching</h4>
          <br />
          <form onSubmit={searchHandler} className="search-form row">
            <div className="input-field col s12 m9 l10">
              <input
                placeholder="Input movie keyword"
                id="first_name"
                type="text"
                value={search}
                onChange={(e) => setSearch(e.target.value)}
              />
              <label htmlFor="first_name" className="active">
                Movie
              </label>
            </div>
            <div className="input-field col s12 m3 l2">
              <button className="btn btn-search" onClick={searchHandler}>
                Search
              </button>
            </div>
          </form>
          <div className="search">
            <div className="row">
              {searchList?.map((movie) => (
                <SearchItem movie={movie} key={movie.id} />
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
