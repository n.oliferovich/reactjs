import { deleteLike } from "../firebase-config";
import { MovieType } from "./MovieList";

export const LikedItem = ({ movie }: { movie: MovieType }) => {
  const deleteLikeFromList = async () => {
    await deleteLike(movie.id).then(() => {
      window.location.reload();
    });
  };

  return (
    <div className="liked-item col s12 m6">
      <div className="card horizontal">
        <div className="card-image">
          <a href={movie.id}>
            <img
              src={
                movie.primaryImage ? movie.primaryImage.url : "/no-image2.jpg"
              }
              alt={movie.titleText.text}
            />
          </a>
          <div
            className="like-movie btn-floating halfway-fab waves-effect waves-light red"
            onClick={deleteLikeFromList}
          >
            <i className="material-icons">remove</i>
          </div>
        </div>
        <div className="card-stacked">
          <div className="card-content">
            <p className="header">{movie.titleText.text}</p>
            {movie.releaseDate && (
              <p>
                Release Date: {movie.releaseDate.day}.{movie.releaseDate.month}.
                {movie.releaseDate.year}
              </p>
            )}
          </div>
          <div className="card-action">
            <a href={movie.id}>Learn more...</a>
          </div>
        </div>
      </div>
    </div>
  );
};
