import { useContext, useState } from "react";
import { MovieType } from "./MovieList";
import { AuthContext } from "../context/AuthContext";
import { setLikes } from "../firebase-config";

export const SearchItem = ({ movie }: { movie: MovieType }) => {
  const context = useContext(AuthContext);
  const [isLikes, setIsLiked] = useState(false);

  const addToLikeList = async () => {
    await setLikes(context.userLogin, movie.id, movie.titleText.text).then(
      () => {
        setIsLiked(true);
      }
    );
  };

  return (
    <div className="search-item col s12 m6">
      <div className="card horizontal">
        <div className="card-image">
          <a href={movie.id}>
            <img
              src={
                movie.primaryImage ? movie.primaryImage.url : "/no-image2.jpg"
              }
              alt={movie.titleText.text}
            />
          </a>
          <div
            className="like-movie btn-floating halfway-fab waves-effect waves-light red"
            onClick={addToLikeList}
          >
            <i className="material-icons">{isLikes ? "remove" : "add"}</i>
          </div>
        </div>
        <div className="card-stacked">
          <div className="card-content">
            <p className="header">{movie.titleText.text}</p>
            {movie.releaseDate && (
              <p>
                Release Date: {movie.releaseDate.day}.{movie.releaseDate.month}.
                {movie.releaseDate.year}
              </p>
            )}
          </div>
          <div className="card-action">
            <a href={movie.id}>Learn more...</a>
          </div>
        </div>
      </div>
    </div>
  );
};
