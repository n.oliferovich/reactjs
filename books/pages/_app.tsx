import { AppProps } from "next/app";
import { useEffect, useState } from "react";
import AppContext from "../context";
import { defaultSorting, SortType } from "../options";
import { getSearchBooks } from "../_http/services/books";
import { Book } from "../_http/responces/book.response";
import "../styles/globals.css";

function MyApp({ Component, pageProps }: AppProps) {
  const [title, setTitle] = useState("");
  const [books, setBooks] = useState<Array<Book>>([]);
  const [searchBooks, setSearchBooks] = useState<Array<Book>>([]);
  const [sortby, setSortby] = useState(defaultSorting);

  const setSearchTitle = (value: string) => {
    setTitle(value);
  };
  const setStateBooks = (value: Book[]) => {
    setBooks(value);
  };
  const setStateSearchBooks = (value: Book[]) => {
    setSearchBooks(value);
  };
  const setSorting = (obj: SortType) => {
    setSortby({ ...sortby, ...obj });
  };

  useEffect(() => {
    if (title.trim()) {
      getSearchBooks(title).then((res) => setSearchBooks(res.items));
    } else {
      setSearchBooks([]);
    }
  }, [title]);

  return (
    <AppContext.Provider
      value={{
        title,
        setSearchTitle,
        books,
        setStateBooks,
        searchBooks,
        setStateSearchBooks,
        sortby,
        setSorting,
      }}
    >
      <Component {...pageProps} />
    </AppContext.Provider>
  );
}

export default MyApp;
