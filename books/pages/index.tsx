import { useContext, useEffect, useMemo } from "react";
import { GetStaticProps } from "next";
import Link from "next/link";
import { Card } from "../components/Card";
import { SortItem } from "../components/SortItem";
import AppContext from "../context";
import { filter, maxResults, orderBy } from "../options";
import { Layout } from "../components/Layout";
import { getFetchBooks } from "../_http/services/books";
import { Book } from "../_http/responces/book.response";
import "react-select-search/style.css";

export default function Home({ fetchBooks }: { fetchBooks: Book[] }) {
  const context = useContext(AppContext);
  const setStateBooks = context.setStateBooks;
  const sortby = context.sortby;
  const books = context.books;

  const booksItems = useMemo(() => {
    return books?.map((book: Book) => <Card book={book} key={book.id} />);
  }, [books]);

  useEffect(() => {
    setStateBooks(fetchBooks);
  }, []);

  useEffect(() => {
    getFetchBooks(sortby);
  }, [sortby]);

  return (
    <Layout>
      <Link href="/search">
        <a className="back-link">
          <span className="back-title">Search</span>
          <span className="back">{">"}</span>
        </a>
      </Link>
      <div className="row-book">
        <div className="row">
          <SortItem options={filter} name="filter" val={filter[0].value} />
          <SortItem
            options={maxResults}
            name="maxResults"
            val={maxResults[0].value}
          />
          <SortItem options={orderBy} name="orderBy" val={orderBy[0].value} />
        </div>

        <h4>Popular books</h4>
        <div className="row">{books?.length && booksItems}</div>
      </div>
    </Layout>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  const res = await getFetchBooks();

  return { props: { fetchBooks: res.items || null } };
};
