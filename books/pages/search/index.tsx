import { useContext, useMemo } from "react";
import Link from "next/link";
import { Card } from "../../components/Card";
import { Form } from "../../components/Form";
import AppContext from "../../context";
import { Layout } from "../../components/Layout";
import { Book } from "../../_http/responces/book.response";

const Search = () => {
  const context = useContext(AppContext);
  const searchBooks = context.searchBooks;

  const searchBooksItems = useMemo(() => {
    return searchBooks?.map((book: Book) => <Card book={book} key={book.id} />);
  }, [searchBooks]);

  return (
    <Layout title="Search Books">
      <Link href="/">
        <a className="back-link">
          <span className="back">{"<"}</span>
          <span className="back-title">Home</span>
        </a>
      </Link>
      <div className="row-book">
        <Form />
        <div className="row">
          {searchBooks?.length ? searchBooksItems : <p>No results</p>}
        </div>
      </div>
    </Layout>
  );
};

export default Search;
