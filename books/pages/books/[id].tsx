import { GetStaticProps, GetStaticPaths } from "next";
import Link from "next/link";
import Image from "next/image";
import bookImg from "../../public/images/book.jpg";
import { Layout } from "../../components/Layout";
import { getBookInfo } from "../../_http/services/books";
import { Book } from "../../_http/responces/book.response";
import { Icon } from "../../icons";

const BookPage = ({ book }: { book: Book }) => {
  return (
    <Layout title={`Book "${book?.volumeInfo.title}"`}>
      <div className="book">
        <Link href="/">
          <a className="back-link">
            <span className="back">{"<"}</span>
            <span className="back-title">Home</span>
          </a>
        </Link>
        <div className="row row-book">
          <div className="col s12 m3">
            <div className="book-image">
              <Image
                src={book ? book.volumeInfo.imageLinks.thumbnail : bookImg}
                alt={book?.volumeInfo.title}
                width="200px"
                height="290px"
              />
            </div>
          </div>
          <div className="col s12 m9">
            <div className="book-content">
              <h4>
                {book?.volumeInfo.title} ({book?.volumeInfo.publishedDate})
              </h4>
              <p>Author(s): {book?.volumeInfo.authors.join(", ")}</p>
              <p>Published by: {book?.volumeInfo.publisher}</p>
              <p>Pages: {book?.volumeInfo.pageCount}</p>
              <p>Rating: {book?.volumeInfo.averageRating}</p>
              <p>Lang: {book?.volumeInfo.language}</p>
              <p>Description: {book?.volumeInfo.description}</p>
            </div>
            <div className="nets">
              {Icon.getFacebookButton(book?.id, book?.volumeInfo.title)}
              {Icon.getTwitterButton(book?.id, book?.volumeInfo.title)}
              {Icon.getVKButton(book?.id)}
              {Icon.getTelegramButton(book?.id)}
              {Icon.getViberButton(book?.id)}
              {Icon.getWhatsappButton(book?.id)}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [],
    fallback: "blocking",
  };
};

export const getStaticProps: GetStaticProps = async ({ params }: any) => {
  const id = params.id;
  const book = await getBookInfo(id);

  return { props: { book: book || null } };
};

export default BookPage;
