export interface Book {
  id: string;
  volumeInfo: {
    title: string;
    publishedDate: string;
    imageLinks: {
      thumbnail: string;
    };
    authors: string[];
    publisher: string;
    pageCount: number;
    averageRating: number;
    language: string;
    description: string;
  };
}
