import { defaultSortType, filter, maxResults, orderBy } from "../options";

export const getPopularBooks = async (sortby: defaultSortType | null = null) => {
  const filters = sortby?.filter || filter[0].value;
  const results = sortby?.maxResults || maxResults[0].value;
  const order = sortby?.orderBy || orderBy[0].value;
  return await fetch(
    `https://www.googleapis.com/books/v1/volumes/?q=book&filter=${filters}&orderBy=${order}&maxResults=${results}`
  );
};

export const getBook = async (id: string) => {
  return await fetch(`https://www.googleapis.com/books/v1/volumes/${id}`);
};

export const searchBook = async (q: string) => {
  return await fetch(
    `https://www.googleapis.com/books/v1/volumes/?q=${q}&maxResults=24`
  );
};
