import { defaultSortType } from "../../options";
import { getBook, getPopularBooks, searchBook } from "../fetches";

export const getFetchBooks = async (sortby: defaultSortType | null = null) => {
  const res = await getPopularBooks(sortby);
  return await res.json();
};

export const getBookInfo = async (id: string) => {
  const res = await getBook(id);
  return await res.json();
};

export const getSearchBooks = async (title: string) => {
  const res = await searchBook(title);
  return await res.json();
};
