import { REACT_APP_BASE_URL } from "./configs";
import {
  FacebookShareButton,
  TelegramShareButton,
  TwitterShareButton,
  ViberShareButton,
  VKShareButton,
  WhatsappShareButton,
} from "react-share";
import {
  FacebookIcon,
  TelegramIcon,
  TwitterIcon,
  ViberIcon,
  VKIcon,
  WhatsappIcon,
} from "react-share";

export class Icon {
  static getFacebookButton(id: string, title: string) {
    return (
      <FacebookShareButton
        url={REACT_APP_BASE_URL + "/books/" + id}
        hashtag={`${title}`}
      >
        <FacebookIcon size={32} round />
      </FacebookShareButton>
    );
  }

  static getTwitterButton(id: string, title: string) {
    return (
      <TwitterShareButton url={REACT_APP_BASE_URL + "/books/" + id}>
        <TwitterIcon size={32} round />
      </TwitterShareButton>
    );
  }

  static getVKButton(id: string) {
    return (
      <VKShareButton url={REACT_APP_BASE_URL + "/books/" + id}>
        <VKIcon size={32} round />
      </VKShareButton>
    );
  }

  static getTelegramButton(id: string) {
    return (
      <TelegramShareButton url={REACT_APP_BASE_URL + "/books/" + id}>
        <TelegramIcon size={32} round />
      </TelegramShareButton>
    );
  }

  static getViberButton(id: string) {
    <ViberShareButton url={REACT_APP_BASE_URL + "/books/" + id}>
      <ViberIcon size={32} round />
    </ViberShareButton>;
  }

  static getWhatsappButton(id: string) {
    <WhatsappShareButton url={REACT_APP_BASE_URL + "/books/" + id}>
      <WhatsappIcon size={32} round />
    </WhatsappShareButton>;
  }
}
