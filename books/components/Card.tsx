import Link from "next/link";
import Image from "next/image";
import bookImg from "../public/images/book.jpg";
import { Book } from "../_http/responces/book.response";

function Card({ book }: {book: Book}) {
  return (
    <div className="col s12 m2">
      <div className="card">
        <div className="card-image">
          <Image
            src={
              book && book.volumeInfo.imageLinks
                ? book.volumeInfo.imageLinks.thumbnail
                : bookImg
            }
            width="190px"
            height="265px"
            alt={book.volumeInfo.title}
          />
          <span className="card-title">{book.volumeInfo.title}</span>
        </div>
        <div className="card-content">
          <p>{book.volumeInfo.publishedDate}</p>
          <p>
            by {book.volumeInfo.authors && book.volumeInfo.authors?.join(", ")}
          </p>
        </div>
        <div className="card-action">
          <Link href={"/books/[id]"} as={`/books/${book.id}`}>
            More...
          </Link>
        </div>
      </div>
    </div>
  );
}

export { Card };
