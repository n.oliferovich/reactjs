import Head from "next/head";
import { FC } from "react";

const Layout: FC<{
  children: React.ReactNode;
  title?: string;
}> = ({ children, title = "Books" }) => {
  return (
    <>
      <Head>
        <title>{title} | Book App</title>
        <meta
          name="description"
          content="Book App for searching, viewing, sorting Google books."
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div className="container">{children}</div>
      </main>

      <footer></footer>
    </>
  );
};

export { Layout };
