import { useContext } from "react";
import AppContext from "../context";

const Form = () => {
  const context = useContext(AppContext);
  const title = context.title;
  const setSearchTitle = context.setSearchTitle;

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTitle(e.target.value);
  };

  return (
    <form className="col s12">
      <div className="row">
        <div className="input-field col s12">
          <input id="title" type="text" value={title} onChange={handleChange} />
          <label className="active" htmlFor="title">
            Book title
          </label>
        </div>
      </div>
    </form>
  );
};

export { Form };
