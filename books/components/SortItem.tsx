import React, { useContext, useState } from "react";
import SelectSearch, { SelectedOptionValue } from "react-select-search";
import AppContext from "../context";
import { SortType } from "../options";

const SortItemComponent = ({
  options,
  name,
  val,
}: {
  options: SortType[];
  name: string;
  val: string;
}) => {
  const context = useContext(AppContext);
  const setSorting = context.setSorting;
  const [value, setValue] = useState(val);

  const handleChange = (selectedValue: any) => {
    setSorting({ [`${name}`]: selectedValue });
    setValue(selectedValue);
  };

  return (
    <div className="col s12 m4">
      <SelectSearch
        options={options}
        value={value}
        onChange={handleChange}
      />
    </div>
  );
};

const SortItem = React.memo(SortItemComponent);

export { SortItem };
