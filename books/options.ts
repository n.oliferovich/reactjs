export type SortType = {
  name: string;
  value: string;
};

const filter: SortType[] = [
  { name: "Partial", value: "partial" },
  { name: "Full", value: "full" },
  { name: "Free-ebooks", value: "free-ebooks" },
  { name: "Paid-ebooks", value: "paid-ebooks" },
  { name: "Ebooks", value: "ebooks" },
];

const maxResults: SortType[] = [
  { name: "10", value: "10" },
  { name: "20", value: "20" },
  { name: "40", value: "40" },
];

const orderBy: SortType[] = [
  { name: "Relevance", value: "relevance" },
  { name: "Newest", value: "newest" },
];

export type defaultSortType = {
  filter: string;
  maxResults: string;
  orderBy: string;
};

const defaultSorting: defaultSortType = {
  filter: filter[0].value,
  maxResults: maxResults[0].value,
  orderBy: orderBy[0].value,
};

export { filter, maxResults, orderBy, defaultSorting };
