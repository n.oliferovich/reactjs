export const increment = () => ({
  type: "INCREMENT",
});

export const decrement = () => ({
  type: "DECREMENT",
});

export const getNews = () => ({
  type: "GET_NEWS",
});
