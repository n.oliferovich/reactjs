import React from "react";
import { connect } from "react-redux";
import img from "../loading_spinner.gif";

let Loading2 = ({ loading }: { loading: any }) =>
  loading ? (
    <div style={{ textAlign: "center" }}>
      <img src={img} alt="loading" />
      <h1>LOADING</h1>
    </div>
  ) : null;

const mapStateToProps = (state: any) => ({ loading: state.loading });
const Loading = connect(mapStateToProps, null)(Loading2);

export default Loading;
