import React from "react";
import { connect } from "react-redux";
import { getNews } from "../_redux/actions/counter.action";

let Button2 = ({ getNews }: {getNews: any}) => (
  <button onClick={getNews}>Press to see news</button>
);
const mapDispatchToProps = {
  getNews: getNews,
};

const Button = connect(null, mapDispatchToProps)(Button2);

export default Button;
