import { useContext } from "react";
import styled, { keyframes, withTheme, ThemeContext } from "styled-components";
import { SongType } from "../App";

const StyledCard = styled.div`
  color: ${(props) => props.theme.border};
  border: 2px solid ${(props) => props.theme.border};
  background: ${(props) => props.theme.back};
`;

const StyledTitle = styled.p`
  color: #000000;
  font-size: 16px;
  line-height: 24px;
  height: 48px;
  overflow: hidden;

  &:hover {
    color: #ffc6dd !important;
    cursor: pointer;
  }
`;

const StyledFullTitle = styled(StyledTitle)`
  color: #ffab40;
`;

const StyledAnimation = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const StyledPlus = styled.a`
  ${StyledCard}: hover & {
    animation: ${StyledAnimation} 2s linear infinite;
  }
`;

const songs = [];

const StyledLink = styled.div`
  background-color: ${songs.length ? "#fff0dd" : "#ffffff"} !important;
`;

const theme = {
  border: "#26a69a",
  back: "#ffffff",
};

const invertTheme = ({ border, back }: { border: any; back: any }) => ({
  border: back,
  back: border,
});

function StyledImg(props: any) {
  return (
    // eslint-disable-next-line jsx-a11y/img-redundant-alt
    <img
      src={props.song.header_image_thumbnail_url}
      alt="Image"
      style={{ backgroundColor: props.theme.back }}
    />
  );
}

const themeName = "link";

const StyledHref = (props: any) => {
  const themeContext = useContext(ThemeContext);

  return <a href={props.url}>This is a {themeContext}</a>;
};

const StyledImgWithTheme = withTheme(StyledImg);

export const Song = ({ song }: { song: SongType }) => {
  return (
    <div className="col s12 m6 l3">
      <StyledCard className="card" theme={theme}>
        <div className="card-image">
          <StyledImgWithTheme song={song} theme={theme} />
          <StyledTitle className="card-title">{song.artist_names}</StyledTitle>
          <StyledPlus
            className="btn-floating halfway-fab waves-effect waves-light red"
            href="https://github.com"
          >
            <i className="material-icons">add</i>
          </StyledPlus>
        </div>
        <div className="card-content">
          <StyledFullTitle>{song.full_title}</StyledFullTitle>
        </div>
        <StyledLink className="card-action" theme={invertTheme}>
          <ThemeContext.Provider value={themeName}>
            <StyledHref href={song.url} />
          </ThemeContext.Provider>
        </StyledLink>
      </StyledCard>
    </div>
  );
};
