import React, { useEffect, useState, useRef } from "react";
import styled from "styled-components";
import axios from "axios";
import "materialize-css";
import "./App.css";
import { Song } from "./components/Song";

export type SongType = {
  id: number;
  url: string;
  artist_names: string;
  full_title: string;
  header_image_thumbnail_url: string;
};

type SearchType = {
  result: {
    id: number;
    url: string;
    artist_names: string;
    full_title: string;
    header_image_thumbnail_url: string;
  };
};

const StyledApp = styled.div`
  background-color: #282c34;
  min-height: 100vh;
  font-size: calc(10px + 2vmin);
  color: white;

  .App-content {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;

const StyledButton = styled.button`
  padding: 0 25px;
  color: ${(props) => props.theme.main};
`;

StyledButton.defaultProps = {
  theme: {
    main: "palevioletred",
  },
};

const theme = {
  main: "white",
};

const StyledInput = styled.input.attrs({
  placeholder: "Song name",
  id: "title",
  type: "text",
})`
  font-size: 18px;
`;

function App() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [songs, setSongs] = useState<Array<SongType>>([]);
  const [search, setSearch] = useState<Array<SearchType>>([]);
  const [title, setTitle] = useState("");

  const refInput = useRef<HTMLInputElement>(null);

  const submitHandler = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setIsLoaded(false);

    if (title) {
      await axios
        .get("https://genius.p.rapidapi.com/search", {
          headers: {
            "x-rapidapi-host": "genius.p.rapidapi.com",
            "x-rapidapi-key":
              "698da318e5msh6a3402e18b4b41bp140b8djsn911e5cdfba01",
          },
          params: { q: title },
        })
        .then((res) => {
          setIsLoaded(true);
          setSongs([]);
          console.log(res.data);
          setSearch(res.data.response.hits);
        })
        .catch((err) => {
          setIsLoaded(true);
          setError(err);
          console.log(err);
        });
    } else {
      getData();
    }
  };

  const handleFocus = () => {
    if (null !== refInput.current) refInput.current.focus();
  };

  const getData = async () => {
    await axios
      .get("https://genius.p.rapidapi.com/artists/16775/songs", {
        headers: {
          "x-rapidapi-host": "genius.p.rapidapi.com",
          "x-rapidapi-key":
            "698da318e5msh6a3402e18b4b41bp140b8djsn911e5cdfba01",
        },
      })
      .then((res) => {
        setIsLoaded(true);
        console.log(res.data);
        setSongs(res.data.response.songs);
      })
      .catch((err) => {
        setIsLoaded(true);
        setError(err);
        console.log(err);
      });
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <StyledApp as="section">
      <div className="container">
        <div className="App-content">
          <h3>Songs</h3>
          <form className="col s12 form-songs" onSubmit={submitHandler}>
            <div className="row search">
              <div className="input-field col m9 s12">
                <StyledInput
                  ref={refInput}
                  className="validate"
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                  onMouseEnter={handleFocus}
                />
                <label className="active" htmlFor="title">
                  Input song
                </label>
              </div>
              <div className="input-field col m3 s12">
                <StyledButton
                  className="waves-effect waves-light btn search-btn"
                  theme={theme}
                >
                  <i className="material-icons">search</i> Search
                </StyledButton>
              </div>
            </div>
          </form>
          {(error && <div>Ошибка: {error}</div>) ||
            (!isLoaded && <div>Загрузка...</div>) || (
              <div className="songs">
                <div className="row">
                  {songs?.map((song) => (
                    <Song song={song} key={song.id} />
                  ))}
                  {search?.map((song) => (
                    <Song song={song.result} key={song.result.id} />
                  ))}
                </div>
              </div>
            )}
        </div>
      </div>
    </StyledApp>
  );
}

export default App;
