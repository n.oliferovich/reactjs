import React from 'react';
import { PostType } from '../_redux/types/types';

const Post = ({ post }: { post: PostType }) => {
  return (
    <div className='col s12'>
      <div className='card blue-grey darken-1'>
        <div className='card-content white-text'>
          <span className='card-title'>Card {post.title}</span>
        </div>
        <div className='card-action'>
          <a href='#'>This is a link</a>
        </div>
      </div>
    </div>
  );
};

export default Post;
