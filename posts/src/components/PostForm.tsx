import React, { useState } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import { hideAlert, showAlert } from '../_redux/actions/app.action';
import { createPost } from '../_redux/actions/post.action';
import { StateType } from '../_redux/types/types';

const PostForm = () => {
  const dispatch = useDispatch();
  const [title, setTitle] = useState('');
  const alert = useSelector((state: StateType) => state.app.alert);

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (!title.trim()) {
      return dispatch(showAlert("Title's empty!"));
    }
    const post = {
      id: Date.now().toString(),
      title,
    };

    dispatch(createPost(post));
    setTitle('');
  };

  return (
    <div>
      {alert && (
        <div className='input-field'>
          <h4 className='alert alert-warning'>{alert}</h4>
        </div>
      )}
      <h1>Post Form</h1>
      <form className='form' onSubmit={handleSubmit}>
        <div className='input-field'>
          <input
            placeholder='Placeholder'
            id='title'
            type='text'
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
          <label className='active' htmlFor='title'>
            Title
          </label>
        </div>

        <div className='input-field'>
          <button className='btn' onClick={handleSubmit}>
            Create
          </button>
        </div>
      </form>
    </div>
  );
};

const mapDispatchToProps = {
  createPost,
};

export default connect(null, mapDispatchToProps)(PostForm);
