import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { PostType, StateType } from '../_redux/types/types';
import Post from './Post';
import { fetchedPost } from '../_redux/actions/post.action';

const FetchedPosts = () => {
  const dispatch = useDispatch();
  const asyncPosts: PostType[] = useSelector(
    (state: StateType) => state.posts.fetchedPosts
  );
  const loading = useSelector((state: StateType) => state.app.loading);

  if (loading) return <div>Loading...</div>;

  if (!asyncPosts.length)
    return (
      <div>
        {' '}
        <button className='btn' onClick={() => dispatch(fetchedPost())}>
          Load
        </button>
      </div>
    );
  return (
    <div className='posts fetched'>
      <div className='row'>
        {asyncPosts.map((post: PostType) => (
          <Post post={post} key={post.id} />
        ))}
      </div>
    </div>
  );
};

export default FetchedPosts;
