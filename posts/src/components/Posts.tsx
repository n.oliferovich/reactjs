import React from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import { PostType, StateType } from '../_redux/types/types';
import Post from './Post';

const Posts = () => {
  const syncPosts: PostType[] = useSelector(
    (state: StateType) => state.posts.posts
  );

  if (!syncPosts.length) return <h4>No posts</h4>;

  return (
    <div className='posts'>
      <div className='row'>
        {syncPosts.map((post: PostType) => (
          <Post post={post} key={post.id} />
        ))}
      </div>
    </div>
  );
};

export default Posts;
