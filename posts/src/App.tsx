import React from 'react';
import FetchedPosts from './components/FetchedPosts';
import PostForm from './components/PostForm';
import Posts from './components/Posts';
import './App.css';

function App() {
  return (
    <div className='App'>
      <div className='container pt-3'>
        <div className='row'>
          <div className='col s12'>
            <PostForm />
          </div>
        </div>
        <div className='row'>
          <div className='col s12 m6'>
            <h1>Posts</h1>
            <Posts />
          </div>
          <div className='col s12 m6'>
            <h1>Fetched Posts</h1>
            <FetchedPosts />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
