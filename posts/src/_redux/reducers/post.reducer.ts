import { CREATE_POST, FETCH_POST } from '../types/action.types';
import { PostsType } from '../types/types';

const initialState: PostsType = {
  posts: [],
  fetchedPosts: [],
};

const postReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case CREATE_POST:
      return {
        ...state,
        posts: [...state.posts, action.payload],
      };
    case FETCH_POST:
      return {
        ...state,
        fetchedPosts: [...state.fetchedPosts, ...action.payload],
      };
    default:
      return {
        ...state,
      };
  }
};

export default postReducer;
