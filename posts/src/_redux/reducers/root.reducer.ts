import { combineReducers } from 'redux';
import appReducer from './app.reducer';
import postReducer from './post.reducer';

const rootReducer = combineReducers({ posts: postReducer, app: appReducer });

export default rootReducer;
