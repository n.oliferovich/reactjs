export type StateType = {
  posts: PostsType;
  app: AppType;
};

export type PostsType = {
  posts: PostType[];
  fetchedPosts: PostType[];
};

export type PostType = {
  id: string;
  title: string;
};

export type AppType = {
  loading: boolean;
  alert: null | string
};
