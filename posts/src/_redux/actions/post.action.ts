import { CREATE_POST, REQUEST_POST } from '../types/action.types';
import { PostType } from '../types/types';

export function createPost(post: PostType) {
  return {
    type: CREATE_POST,
    payload: post,
  };
}

export function fetchedPost() {
  return {
    type: REQUEST_POST,
  }
}
