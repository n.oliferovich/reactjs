import { hideAlert, showAlert } from '../actions/app.action';
import { CREATE_POST } from '../types/action.types';

const forbidden = ['fuck', 'spam'];

export function forbiddenWordMiddleware({ dispatch }: { dispatch: any }) {
  return function (next: any) {
    return function (action: any) {
      if (action.type === CREATE_POST) {
        const found = forbidden.filter((w: string) =>
          action.payload.title.includes(w)
        );
        if (found.length) {
          return dispatch(showAlert("It's spam!"));
        } else {
          dispatch(hideAlert());
        }
      }
      return next(action);
    };
  };
}
