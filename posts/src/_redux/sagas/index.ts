import { call, put, takeEvery } from 'redux-saga/effects';
import { hideLoader, showAlert, showLoader } from '../actions/app.action';
import { FETCH_POST, REQUEST_POST } from '../types/action.types';
import { PostType } from '../types/types';

export function* sagaWatcher() {
  yield takeEvery(REQUEST_POST, sagaWorker);
}

function* sagaWorker() {
  try {
    yield put(showLoader());
    const payload: Array<PostType> = yield call(fetchPosts);
    yield put({ type: FETCH_POST, payload });
    yield put(hideLoader());
  } catch (err: any) {
    // yield put(showAlert('Server error'));
    yield put(hideLoader());
  }
}

async function fetchPosts(): Promise<Array<PostType>> {
  const response = await fetch(
    'https://jsonplaceholder.typicode.com/posts?_limit=5'
  );

  return await response.json();
}
